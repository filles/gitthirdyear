
void playMessage(size_t index)
{
    if (is_sufficient==Battery::IsSufficientlyHigh())
        {
            msg=MessageMemory::GetMessage(index);
            int i=0;
            block=msg.begin();
            while(block!=msg.end())
            {
                Speaker::PlayAudioBLock(block);
                block++;
            }
            UserInterface::Display("-");
            UserInterface::Display("-");
        }
    return;
}