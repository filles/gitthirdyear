
#include "Message.h"
//#include "headers/Message.h"
#include <climits>
#include <sstream>
#include <array>

using namespace jsonxx;

Message::Message(const std::string& msg)
{
    Object jsonParser;
    jsonParser.parse(msg);
    deviceId = jsonParser.get<Number>("deviceId");
    data = jsonParser.get<Number>("data");
    deviceType = jsonParser.get<String>("deviceDescription");
    if (deviceType=="Bloodpressure Monitor"){
        sType = BloodPressure;
    }else if( deviceType == "Thermometer"){
        sType = Temperature;
    }
    else if(deviceType=="Substancelevel Monitor"){
        sType = SubstanceLevel;
    }
    else if(deviceType == "Heartrate Monitor"){
        sType = HeartRate;
    }
    timestamp = std::chrono::steady_clock::now();
}

//int main(){
//    std::string a = "{'deviceId': 1, 'deviceDescription': 'Thermometer', 'data': 14.566925510413032}";
//    Message msg(a);
//    return 0;
//}