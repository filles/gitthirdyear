#ifndef CAS_HMMONITORCONTROLLER_HPP
#define CAS_HMMONITORCONTROLLER_HPP

#include "Action.hpp"
#include "Config.hpp"
#include "services.hpp"
#include "tcpService.hpp"
#include <string>
#include <unordered_map>

using namespace std;

class HmMonitorController
{
public:
    HmMonitorController(Config conf); // constructor
    // HmServices[] services;
    std::unordered_map<serviceType, service*> serv;
    Config config;
    TcpService* tcpService;
    // service serv[4];
    int run();
    void init();

    void takeAction(Action todo);
    // void checkincomingMessage(string msg);
    void update(string msg);
    void receive(string msg);
    void sendMessage(string msg);

    bool* checkIfActionRequired(Message msg);

private:
    unsigned inputPort;
    unsigned outputPort;
};

#endif // CAS_HMMONITORCONTROLLER_HPP