#ifndef CAS_CONFIG_H
#define CAS_CONFIG_H

#include <string>
#include <array>

class Config {
public:
    Config(int incoming, int outgoing);

    int incomingPort=8005;
    int outgoingPort=8002;

    float criticalBloodPressure;
    float criticalBodyTemperature;
    float criticalHeartRate;
    float criticalSubstanceLevel; // cholesterol only
    //float criticalSubstanceLevel[3]; // {cholesterol, oxygen, alcohol}
};

#endif //close ifndef
