#ifndef CAS_ACTION_H
#define CAS_ACTION_H

#include <string>

class Action {

  public:
  Action(std::string state_init, int deviceTo_init);

  int deviceTo;
  std::string state;

  std::string call_help();


};

#endif //close ifndef
