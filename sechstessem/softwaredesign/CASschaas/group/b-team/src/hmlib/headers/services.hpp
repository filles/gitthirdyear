#ifndef CAS_SERVICES_H
#define CAS_SERVICES_H


//#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include "Message.h"
#include "Action.hpp"
#include "Config.hpp"

using namespace std;

typedef std::chrono::steady_clock::time_point TIMESTAMP;

struct msg_struct{
    int id;
    double value;
    std::chrono::steady_clock::time_point timestamp;
};

class service {
    public:
    service();
    ~service(); // deconstructor
    //class attributes
    vector<msg_struct> messages; //stores the received messages
    const static vector<int> HMServiceList;// stores a List of Services
    virtual Action update(Message msg);
    virtual Action checkRequiredActions(Message msg);
    vector<Action> returnactionList(Action);
    void create_actions() ;
    double threshhold;
};

class ServiceBloodPressure : public virtual service{
    public:
    Action update(Message msg);
    ServiceBloodPressure(Config conf);
    double getAvgBloodPressure(int id, TIMESTAMP from, TIMESTAMP to);
    Action checkRequiredActions(Message msg);
};


class ServiceBodyTemperature : public virtual service{
    public:
    Action update(Message msg);
    ServiceBodyTemperature(Config conf);
    double getAvgBodyTemp(int id, TIMESTAMP from, TIMESTAMP to);
    Action checkRequiredActions(Message msg);
};

class ServiceSubstanceLevel  : public virtual service{
    public:
    Action update(Message msg);
    ServiceSubstanceLevel(Config conf);
    Action checkRequiredActions(Message msg);
};

class ServiceHeartRate  : public virtual service{
    public:
    Action update(Message msg);
    ServiceHeartRate(Config conf);
    double getAvgHeartRate(int id, TIMESTAMP from, TIMESTAMP to);
    Action checkRequiredActions(Message msg);
};

#endif