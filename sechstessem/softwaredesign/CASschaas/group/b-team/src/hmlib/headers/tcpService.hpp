#ifndef TCP_SERVICE_H
#define TCP_SERVICE_H

#include <vector>
#include <string>
// #include "../PracticalSocket.cpp"
#include "PracticalSocket.h"

class TcpService
{
    public:
    TcpService();
    TcpService(unsigned short input, unsigned short output);
    std::string checkIncomingMessages();
    void sendMessages(std::string msg);
    private:
    
    unsigned short incomingPort;
    unsigned short outgoingPort;
    TCPSocket* incomingSocket;
    TCPSocket* outgoingSocket;
    void getSocket(TCPSocket* sock);
};


#endif //TCP_SERVICE_H