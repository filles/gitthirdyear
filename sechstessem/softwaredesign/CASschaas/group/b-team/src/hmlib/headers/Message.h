#ifndef HEALTHDEVICEMONITOR_MESSAGE_H
#define HEALTHDEVICEMONITOR_MESSAGE_H

#include <chrono>
#include <ctime>
#include "jsonxx.h"
#include <string>
#include <vector>

enum serviceType{
    BloodPressure=0,
    Temperature=1,
    SubstanceLevel=2,
    HeartRate=3
};

class Message
{
public:
    int deviceId;
    double data;
    std::string deviceType;
    serviceType sType;
    std::chrono::steady_clock::time_point timestamp;
    explicit Message(const std::string& msg);

    //Message(std::istream &json_string); //this constructor is never used...

};

#endif // HEALTHDEVICEMONITOR_MESSAGE_H