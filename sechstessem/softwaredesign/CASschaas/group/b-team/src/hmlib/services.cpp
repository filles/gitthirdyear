#include "services.hpp"

 //class operations
service::service(){
        
}

service::~service(void)
{cout << "Service is being deleted" << endl;}

Action service::update(Message msg) //: takes a json converts it to string and updates Values
    {
        msg_struct arr;
        arr.id = msg.deviceId;
        arr.value = msg.data;
        arr.timestamp = msg.timestamp;
        messages.push_back(arr);
        Action todo = checkRequiredActions(msg);
        return todo;
    }  

Action service::checkRequiredActions(Message msg) //: checks for a given message which action should be taken
    {

        cout << "the necessary actions: ... " << endl;
    }
    
vector<Action> service::returnactionList(Action bloob)//: returns actionList
    {
        
    }
void service::create_actions() //: calls actions List
 {
    cout << "the created actions: ... " << endl;
 }
 ///////////////////////////////////////////////////////

ServiceBloodPressure::ServiceBloodPressure(Config conf)
{
    threshhold = conf.criticalBloodPressure;
}
Action ServiceBloodPressure::update(Message msg) //: takes a json converts it to string and updates Values
    {
        msg_struct arr;
        arr.id = msg.deviceId;
        arr.value = msg.data;
        arr.timestamp = msg.timestamp;
        messages.push_back(arr);
        Action todo = checkRequiredActions(msg);
        return todo;
    }  

double ServiceBloodPressure::getAvgBloodPressure(int id, TIMESTAMP from, TIMESTAMP to)//: gets the average blood pressure
{
    int size = messages.size();
    int count=0;
    double value;

    for (int i = 0; i < size; i++)
    {
	if((messages[i].timestamp >= from) && (messages[i].id==id))
		{
			value=value+ messages[i].value;
            count=count+1;
		}
    }
    value=value/count;
    return value;
}

Action ServiceBloodPressure::checkRequiredActions(Message msg)
{
    // check if blood pressure above critical
    Action todo("good", msg.deviceId);
    TIMESTAMP now= std::chrono::steady_clock::now();
    TIMESTAMP t1 = now - std::chrono::seconds(5);
    double data=getAvgBloodPressure(msg.deviceId, t1, now);
    if (data > threshhold){
        todo.state="Blood pressure is too high!";
    }
    return todo;
}

//////////////////////////////////////////////////////////
ServiceBodyTemperature::ServiceBodyTemperature(Config conf)
{
    threshhold = conf.criticalBodyTemperature;
}
Action ServiceBodyTemperature::update(Message msg) //: takes a json converts it to string and updates Values
    {
        msg_struct arr;
        arr.id = msg.deviceId;
        arr.value = msg.data;
        arr.timestamp = msg.timestamp;
        messages.push_back(arr);
        Action todo = checkRequiredActions(msg);
        return todo;
    }  

double ServiceBodyTemperature::getAvgBodyTemp(int id,TIMESTAMP from, TIMESTAMP to) //: gets the average body temperature
{
    int size = messages.size();
    int count=0;
    double value;

    for (int i = 0; i < size; i++)
    {
	if((messages[i].timestamp >= from) && (messages[i].id==id))
		{
			value=value+ messages[i].value;
            count=count+1;
		}
    }
    value=value/count;
    return value;
}
Action ServiceBodyTemperature::checkRequiredActions(Message msg)
{   // check if blood pressure above critical
    Action todo("good", msg.deviceId);
    TIMESTAMP now= std::chrono::steady_clock::now();
    TIMESTAMP t1 = now - std::chrono::seconds(10);
    double data = getAvgBodyTemp(msg.deviceId, t1, now);
    std::cout << "data:" << data << std::endl;
    if (data > threshhold)
    {todo.state="Temperature is too high!";}
    return todo;
}
////////////////////////////////////////////////////////////
ServiceSubstanceLevel::ServiceSubstanceLevel(Config conf)
{
    threshhold = conf.criticalSubstanceLevel;
    /*threshhold[0] = conf.criticalSubstanceLevel[0];
    threshhold[1] = conf.criticalSubstanceLevel[1];
    threshhold[2] = conf.criticalSubstanceLevel[2];*/
}
Action ServiceSubstanceLevel::update(Message msg) //: takes a json converts it to string and updates Values
    {
        msg_struct arr;
        arr.id = msg.deviceId;
        arr.value = msg.data;
        arr.timestamp = msg.timestamp;
        messages.push_back(arr);
        Action todo = checkRequiredActions(msg);
        return todo;
    }  

Action ServiceSubstanceLevel::checkRequiredActions(Message msg)
{
    // check if substance level above critical
    Action todo("good", msg.deviceId);
    //if (msg.data[0] > threshhold[0] || msg.data[1] > threshhold[1] || msg.data[2] > threshhold[2])
    if (msg.data > threshhold)
    {todo.state="Substance level is above threshold";}
    return todo;
}

//////////////////////////////////////////////////////////////
ServiceHeartRate::ServiceHeartRate(Config conf)
{
    threshhold = conf.criticalHeartRate;
}
Action ServiceHeartRate::update(Message msg) //: takes a json converts it to string and updates Values
    {
        msg_struct arr;
        arr.id = msg.deviceId;
        arr.value = msg.data;
        arr.timestamp = msg.timestamp;
        messages.push_back(arr);
        Action todo = checkRequiredActions(msg);
        return todo;
    }  

double ServiceHeartRate::getAvgHeartRate(int id,TIMESTAMP from, TIMESTAMP to) //: gets the average heart rate
{
    int size = messages.size();
    int count=0;
    double value;

    for (int i = 0; i < size; i++)
    {
	if((messages[i].timestamp >= from) && (messages[i].id==id))
		{
			value=value+ messages[i].value;
            count=count+1;
		}
    }
    value=value/count;
    return value;
}

Action ServiceHeartRate::checkRequiredActions(Message msg)
{   // check if blood pressure above critical
    Action todo("good", msg.deviceId);
    TIMESTAMP now= std::chrono::steady_clock::now();
    TIMESTAMP t1 = now - std::chrono::milliseconds(100);
    double data =getAvgHeartRate(msg.deviceId, t1, now);
    if (data > threshhold)
    {todo.state="The patient is tachycardic";}
    return todo;
}