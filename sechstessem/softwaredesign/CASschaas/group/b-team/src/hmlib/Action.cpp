#include "Action.hpp"
#include <string>
#include "jsonxx.h"
#include <sstream>

using namespace jsonxx;

Action::Action(std::string state_init, int deviceTo_init) {
  state = state_init;
  deviceTo = deviceTo_init;
}

std::string Action::call_help() {
    std::string jsonMsg = "{'deviceId':";
    jsonMsg += std::to_string(deviceTo);
    jsonMsg += ", 'state': " + state +"}";
    return jsonMsg;
}
