#include "Config.hpp"

Config::Config(int incoming, int outgoing): incomingPort{incoming}, outgoingPort{outgoing}
{
    // define critical vital signs here
    //criticalBloodPressure[0] = 140;
    criticalBloodPressure = 90; // this corresponds to a blood pressure of 140/90mmHg

    criticalBodyTemperature = 37.1; // in °C

    criticalHeartRate = 100; // in beats per minute

    criticalSubstanceLevel = 130; // cholesterol in mg/dL
    /*criticalSubstanceLevel[0] = 130; // cholesterol in mg/dL
    criticalSubstanceLevel[1] = 0.9; // oxygen saturation (a number between 0 and 1)
    criticalSubstanceLevel[2] = 0.5; // blood alcohol (a number between 0 and 1)*/
}
