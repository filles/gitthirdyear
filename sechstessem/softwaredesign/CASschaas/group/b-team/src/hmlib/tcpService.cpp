// #include "headers/tcpService.hpp"
#include "tcpService.hpp"

#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>


const unsigned int RCVBUFSIZE = 150;    // Size of receive buffer

TcpService::TcpService(): incomingPort{0}, outgoingPort{0} {}

TcpService::TcpService(unsigned short input, unsigned short output): incomingPort{input}, outgoingPort{output}
{
  std::cout << "trying to connect!" << std::endl;
  try {
    TCPServerSocket inServSock(incomingPort);     // Server Socket object
    incomingSocket = inServSock.accept();
    getSocket(incomingSocket);  // Wait for a client to connect
  } catch (SocketException &e) {
    std::cerr << e.what() << std::endl;
    exit(1);
  }
  std::cout << "in connected!" << std::endl;
  // try {
  //   outgoingSocket = new TCPSocket("localhost",incomingPort);     // Server Socket object
  //   getSocket(outgoingSocket);  // Wait for a client to connect
  // } catch (SocketException &e) {
  //   std::cerr << e.what() << std::endl;
  //   exit(1);
  // }
  // std::cout << "out connected!" << std::endl;
  
}

void TcpService::getSocket(TCPSocket* sock)
{
  std::cout << "Handling client ";
  try {
    std::cout << sock->getForeignAddress() << ":";
  } catch (SocketException e) {
    std::cerr << "Unable to get foreign address" << std::endl;
  }
  try {
    std::cout << sock->getForeignPort();
  } catch (SocketException e) {
    std::cerr << "Unable to get foreign port" << std::endl;
  }
  std::cout << std::endl;

}

std::string TcpService::checkIncomingMessages()
{    
  char echoBuffer[RCVBUFSIZE];
  int recvMsgSize;
  std::string recvMsg;
  std::cout << "checkIncomingMessages: " << std::endl;
  if((recvMsgSize = incomingSocket->recv(echoBuffer, RCVBUFSIZE)) > 0)
  {
    std::string a(echoBuffer, 0, recvMsgSize);
    recvMsg=a;
    std::cout << "size: "<< recvMsgSize <<" msg: " << recvMsg << std::endl;
  }
  return recvMsg;
}

void TcpService::sendMessages(std::string msg)
{
//  const char *cmsg = msg.c_str();
//  // incomingSocket->send(cmsg,msg.length());
//  outgoingSocket->send(cmsg, msg.length());
//  std::cout << "message sent!" <<std::endl;
  std::cout << "sending: " << msg << std::endl;
  std::ofstream logFile;
  logFile.open("../state.out", std::ios::app);
  logFile << msg << std::endl;
  logFile.close();
}


// int main()
// {
//     TcpService test(8005,8002);
//     auto msg = test.checkIncomingMessages();
//     std::cout << "converted msg: " << msg << std::endl;
//     // std::this_thread::sleep_for(std::chrono::seconds(5));
//     // for(int i = 0; i < 10; i++){
//     //   test.checkIncomingMessages();
//     // }
//     // test.checkIncomingMessages();
//     // test.sendMessages(1,"hello");
//     return 0;
// }