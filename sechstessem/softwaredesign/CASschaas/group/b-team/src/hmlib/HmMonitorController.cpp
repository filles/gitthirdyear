#include "HmMonitorController.hpp"
#include "Message.h"
#include "services.hpp"
#include "tcpService.hpp"
#include "Action.hpp"
#include <string>


HmMonitorController::HmMonitorController(Config conf) :
    config{conf},
    inputPort{static_cast<unsigned int>(conf.incomingPort)},
    outputPort{static_cast<unsigned int>(conf.outgoingPort)} {}

void HmMonitorController::init()
{
    // blood pressure, temperature, substance level, heart rate
    tcpService = new TcpService(inputPort, outputPort);
    std::cout <<"connected!" <<std::endl;
    // enum serviceType {
    //     BloodPressure = 0,
    //     Temperature = 1,
    //     SubstanceLevel = 2,
    //     HeartRate = 3
    // };

    serv[BloodPressure] = new ServiceBloodPressure(config);
    serv[Temperature] = new ServiceBodyTemperature(config);
    serv[SubstanceLevel] = new ServiceSubstanceLevel(config);
    serv[HeartRate] = new ServiceHeartRate(config);
}

int HmMonitorController::run() {
    //contains the ~main~ loop: More information in the sequence diagram
    for(;;)
    {        
        Message msg(tcpService->checkIncomingMessages()); //works

        Action action = serv[msg.sType]->update(msg); // return todo action

        std::cout << "action created" << std::endl;

        if(action.state != "good"){
            std::string msg = action.call_help();
            tcpService->sendMessages(msg);
            std::cout << "sent message" << std::endl;
        }
    }
    return 0;
}
