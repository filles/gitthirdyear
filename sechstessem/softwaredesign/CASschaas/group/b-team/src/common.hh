#ifndef _COMMON_HH_
#define _COMMON_HH_

// STATUS MESSAGES
#define STATUS_OK 0
#define STATUS_BAD 0xFF

// Here you can have all your common flags/macros/preprocessor-definitions that are used in all packages of the project.


#define VERBOSE 0
#define DEBUG 1

#endif
