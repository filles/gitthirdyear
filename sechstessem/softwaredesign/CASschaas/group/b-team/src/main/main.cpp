//#include <../common.hh>
#include <iostream>
//#include <../packageExample/dummyClass.h>
#include "HmMonitorController.hpp"
#include "Config.hpp"


int main(int argc, char *argv[])
{
    // CALL MAIN EVENT HANDLER
    std::cout << "starting!" << std::endl;
    int in = 8005; int out = 8002;
    Config conf(in, out);
    std::cout << "read config" << std::endl;
    HmMonitorController controller(conf);
    std::cout << "controller constructed" << std::endl;
    controller.init();
    std::cout << "controller initalized" << std::endl;
    controller.run();

    // SIGNAL TO STOP RECEIVED => STOP EXECUTION
    return 0;
}
