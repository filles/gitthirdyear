import pandas as pd
# Python 3 program to find the longest repeated 
# non-overlapping substring 
# Returns the longest repeating non-overlapping 
# substring in str 
'''
def longestRepeatedSubstring(str): 
  
    n = len(str) 
    LCSRe = [[0 for x in range(n + 1)]  
                for y in range(n + 1)] 
  
    res = "" # To store result 
    res_length = 0 # To store length of result 
  
    # building table in bottom-up manner 
    index = 0
    for i in range(1, n + 1): 
        for j in range(i + 1, n + 1): 
              
            # (j-i) > LCSRe[i-1][j-1] to remove 
            # overlapping 
            if (str[i - 1] == str[j - 1] and
                LCSRe[i - 1][j - 1] < (j - i)): 
                LCSRe[i][j] = LCSRe[i - 1][j - 1] + 1
  
                # updating maximum length of the 
                # substring and updating the finishing 
                # index of the suffix 
                if (LCSRe[i][j] > res_length): 
                    res_length = LCSRe[i][j] 
                    index = max(i, index) 
                  
            else: 
                LCSRe[i][j] = 0
  
    # If we have non-empty result, then insert  
    # all characters from first character to  
    # last character of string 
    if (res_length > 0): 
        for i in range(index - res_length + 1, 
                                    index + 1): 
            res = res + str[i - 1] 
  
    return res 
'''

if __name__ == "__main__": 
      
    #str = open('characters.bd2a09f6.txt','r')
    #str2 = open('crypto.204cfa7f.txt','r')
    '''with open('names.6020a082.txt','r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0;
        for row in csv_reader:
            if line_count = 0:
                line_count +=1;
    '''

    df = pd.read_csv('namesandsal.csv')
    print(df)

    #print (" ayy")
    #df2 = pd.read_csv('salary.c693064e.txt','r')
    #print(df2.columns)
    #strin = str2.read()
    #stris = str3.read()
    #print(longe#stRepeatedSubstring(stri)) 
    #print(strin)
    #print(stris)
  
