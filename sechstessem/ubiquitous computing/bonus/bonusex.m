"hello ubicomp test " 
%ecg = import data('data-ecg.csv');
ecg = readmatrix('data-ecg.csv');

%plot(ecg);

fs = 128;
samples_per_inter = fs*15;


length(ecg)/samples_per_inter;

ni = 8*60/15;
n = length(ecg);

dur_in_sec = n/fs;
dur_in_min = dur_in_sec/60;
factor = 4;

bpms = zeros(ni,1);
%divide beats by sig duration per min to get bpm
for currinter = 1 : 8*60/15
    bc = 0;
    for j = 1 : 127
        k = currinter + j;
       if(ecg(k) > ecg(k-1) & ecg(k) > ecg(k+1) & ecg(k) > 1 )
          bc = bc + 1;
       end
    end
    
bpms(currinter)=  bc/factor;
BPM =  bc/factor;
end
bpms