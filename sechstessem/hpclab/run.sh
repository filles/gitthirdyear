#!/bin/bash
echo "starting bash script ..."
for cores in {1,2,4,8,16,32,64}
do
    for size in {64,128,256,512,1024}
    do
        export OMP_NUM_THREADS=$cores;
        echo "running simulation with $cores cores and size=$size x $size"
        bsub -n $cores -W 01:00 -R "span[ptile=$cores]" ./main $size 100 0.01
        echo "c $cores & s $size finished"
    done
done
