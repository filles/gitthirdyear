"""
Collection of linear algebra operations and CG solver
"""
from mpi4py import MPI
import numpy as np
from . import data
from . import operators
import math

"""Computes the inner product of x and y"""
def hpc_dot(x, y):
    #n = y.domain.local_ny
    result = np.zeros(1)
    globres = np.zeros(1)
    #print(x)
    #print(x.inner)
    #for i in range(n):
    #    result = result + x.inner.flatten()[i]*y.inner.flatten()[i]
    #x.comm_cart.Allgather([result,MPI.INT],[globres,MPI.INT])
    #x.comm_cart.Allreduce([result,MPI.INT],[globres,MPI.INT],MPI.SUM)
    #globres =x.domain.comm_cart.allreduce(result,MPI.SUM)
    result[0]= np.dot(x.inner.flatten(),y.inner.flatten())
    x.domain.comm_cart.Allreduce([result,MPI.DOUBLE],[globres, MPI.DOUBLE], MPI.SUM )
	
    #print(str(globres) + " ths os globres \n")
    return globres

"""Computes the 2-norm of x"""
def hpc_norm2(x):
    #assumes quadratic dims
    '''n = x.domain.local_nx
    result = 0
    globres = 0
    for i in range(n):
        result = result + x.inner.flatten()*x.inner.flatten()]
    #x.domain.comm_cart.Allreduce([result,MPI.INT],[globres,MPI.INT],MPI.SUM)
    globres = x.domain.comm_cart.allreduce(result,MPI.SUM)
    #globres = x.domain.comm_cart.Allgather([result,MPI.INT])
    print(str(globres) + ' glores from norm / res : '+ str(result ))
    sol = np.sqrt([globres])'''
    solw = math.sqrt(hpc_dot(x,x))
    return solw

class hpc_cg:
    """Conjugate gradient solver class: solve the linear system A x = b"""
    def __init__(self, domain):
        self._Ap = data.Field(domain)
        self._r  = data.Field(domain)
        self._p  = data.Field(domain)

        self._xold  = data.Field(domain)
        self._v  = data.Field(domain)
        self._Fxold  = data.Field(domain)
        self._Fx  = data.Field(domain)
        self._v  = data.Field(domain)

    def solve(self, A, b, x, tol, maxiter):
        """Solve the linear system A x = b"""
        # initialize
        A(x, self._Ap)
        self._r.inner[...] = b.inner[...] - self._Ap.inner[...]
        self._p.inner[...] = self._r.inner[...]
        delta_kp = hpc_dot(self._r, self._r)
 
        # iterate
        converged = False
        for k in range(0, maxiter):
            delta_k = delta_kp
            if delta_k < tol**2:
                converged = True
                break
            A(self._p, self._Ap)
            alpha = delta_k/hpc_dot(self._p, self._Ap)
            x.inner[...] += alpha*self._p.inner[...]
            self._r.inner[...] -= alpha*self._Ap.inner[...]
            delta_kp = hpc_dot(self._r, self._r)
            self._p.inner[...] = ( self._r.inner[...]
                                  + delta_kp/delta_k*self._p.inner[...] )

        return converged, k + 1

