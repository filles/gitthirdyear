from mpi4py import MPI

# get comm, size & rank
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# hello
#print(f"Hello world from rank {rank} out of {size} processes")
ndim = 2
dims = MPI.Compute_dims(size, [0]*ndim)
periods = [True] *len(dims);
topo = comm.Create_cart(dims,periods=periods)
coordinates = topo.coords

'''
for i in range(ndim):
	for p in (-1,+1):
		coord = list(coordinates)
		coord[i] = (coord[i]+d)%dims[i]
		neigh = topo.Get_cart_rank(coord)
		source,dest = topo.Shift(i,d)
		neighbours.append(neigh) 
'''
north, south = topo.Shift(0,+1)
east , west  = topo.Shift(1,1)

#print("rank is: " + str(rank) + " and north / south are " \
#+ str(north) + " / " + str(south)+ " and finally east / west " \
#+ str(east) + " / " + str(west) )


#Solution for 2.3 :
re1 = comm.isend(rank, dest = north, tag =  1)
re2 = comm.isend(rank, dest = south, tag =  2)
re3 = comm.isend(rank, dest = east, tag =  3)
re4 = comm.isend(rank, dest = west, tag =  4)

req1 = comm.irecv(source=north, tag = 2)
req2 = comm.irecv(source=south, tag = 1)
req3 = comm.irecv(source=east , tag = 4)
req4 = comm.irecv(source=west , tag = 3)

rcv1 = req1.wait();
rcv2 = req2.wait();
rcv3 = req3.wait();
rcv4 = req4.wait();

#print("rank is: " + str(rank) + " and north / south are " \  
#+ str(rcv1 ) + " / " + str(rcv2 )+ " and finally east / west " \  
#+ str(rcv3) + " / " + str(rcv4))

print("\nrank: " + str(rank) + "\nnorth: "+ str(north) + " / " + str(rcv1 )+"\n" \
+ "south: " + str(south)+ " / " + str(rcv2 ) + "\neast: " \
+ str(east) + " / " + str(rcv3) + "\nwest: " + str(west) + " / " + str(rcv4)) 




MPI.Finalize()
