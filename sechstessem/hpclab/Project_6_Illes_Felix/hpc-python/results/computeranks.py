from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

up = (rank + 1 ) % size;
do = (rank + size - 1 ) % size;

currsum = rank;
sendbuf = rank;
#rcvbuf = rank;
#print( do, rank, up )

for i in range(size) :
	# send to up rank
	#first version with slow :
	comm.send(sendbuf, dest = up, tag =  i)
	rcv = comm.recv(source=do, tag = i)
	#MPI.Send(sendbuf,1,MPI_INT,up,1,MPI_COMM_WORLD);
	#MPI.Recv(rcvbuf,1,MPI_INT,down,MPI_COMM_WORLD);
	currsum += rcv;
	sendbuf = rcv;
	
if rank == 0 :
	print(str(currsum) + " is the sum " );
MPI.Finalize();
