from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

up = (rank + 1 ) % size;
do = (rank + size - 1 ) % size;

currsum = rank;
sendbuf = rank;
recvbuf = rank;
#rcvbuf = rank;
#print( do, rank, up )

for i in range(size) :
	# send to up rank
	#first version with slow :
	sendbuf = np.array(recvbuf,dtype='i')
	recvbuf = np.empty(1,dtype='i')
	if rank == 0:
		comm.Recv([recvbuf, MPI.INT],source = do, tag = i)
		comm.Send([sendbuf,MPI.INT], dest = up, tag =  i)
	else:
		comm.Send([sendbuf,MPI.INT], dest = up, tag =  i)
		comm.Recv([recvbuf, MPI.INT],source=do, tag = i)
	#comm.Send(sendbuf, dest = up, tag =  i)
	#comm.Recv(recvbuf,source=do, tag = i)
	#MPI.Send(sendbuf,1,MPI_INT,up,1,MPI_COMM_WORLD);
	#MPI.Recv(rcvbuf,1,MPI_INT,down,MPI_COMM_WORLD);
	currsum += recvbuf;
	sendbuf = recvbuf;
	
if rank == 0 :
	print(str(currsum) + " is the sum " );
MPI.Finalize();
