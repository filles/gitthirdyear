#!/bin/bash
echo "starting bash script ..."
for cores in {2..32..3}
do
    #for size in {128,256,512,1024}
    for size in {128,256}
    do
   #  export OMP_NUM_THREADS=$cores;
        echo "running simulation with $cores cores and size=$size x $size"
        bsub -n $cores -W 01:00 -R "span[ptile=$cores]" mpirun ./main $size 100 0.01
        
	#echo "c $cores & s $size finished"
    done
done
