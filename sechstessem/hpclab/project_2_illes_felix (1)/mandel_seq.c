#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#include "pngwriter.h"
#include "consts.h"


unsigned long get_time ()
{
    struct timeval tp;
    gettimeofday (&tp, NULL);
    return tp.tv_sec * 1000000 + tp.tv_usec;
}

int main (int argc, char** argv)
{
	png_data* pPng = png_create (IMAGE_WIDTH, IMAGE_HEIGHT);
	
	double  cx, cy, zr, zi, temp;
	cy = MIN_Y;
	
	double fDeltaX = (MAX_X - MIN_X) / (double) IMAGE_WIDTH;
	double fDeltaY = (MAX_Y - MIN_Y) / (double) IMAGE_HEIGHT;
	
	long nTotalIterationsCount = 0;
	unsigned long nTimeStart = get_time ();
	
	long i, j, n, currmax;
	double iters[MAX_ITERS] = {0};
        
        n = 0;
	// do the calculation
	#pragma omp parallel for
	for (j = 0; j < IMAGE_HEIGHT; j++)
	{
		cx = MIN_X;
		#pragma omp parallel for
		for (i = 0; i < IMAGE_WIDTH; i++)
		{
			// compute the orbit z, f(z), f^2(z), f^3(z), ...
			// count the iterations until the orbit leaves the circle |z|=2.
			// stop if the number of iterations exceeds the bound MAX_ITERS.
		        // >>>>>>>> CODE IS MISSING
			zr = zi = 0;
			// here we use that |z| < 2 -> z^2 < 4 
			//while( zr*zr*zr*zr+zi*zi*zi*zi- 6*zi*zi*zr*zr+
				//4*zr*zr*zr*zi- 4*zr*zi*zi*zi+ 4*zr*zr*zr*zi < 4 && n < MAX_ITERS){
			while(abs(zr+zi) < 2 && n < MAX_ITERS){

			/*	
			#pragma omp parallel for shared(currmax)
			for(int x = MAX_ITERS; x > 0; --x){
				
				if( abs(zr+zi) < 2){
	 			iters[
				}
			*/
					temp = zr;
	 				zr = zr*zr-zi*zi+cx;
					zi = 2*temp*zi+cy; 
					n+=1;
					//printf("%s \n"," we looping boys" );
			}
					//printf("%s \n"," we endet looping boys" );

                        // <<<<<<<< CODE IS NISSING
                        // n indicates if the point belongs to the mandelbrot set
			// plot the number of iterations at point (i, j)
			int c = ((long) n * 255) / MAX_ITERS;
			png_plot (pPng, i, j, c, c, c);
			cx += fDeltaX;
			nTotalIterationsCount += n;
			n = 0 ; 
		}
		cy += fDeltaY;
	}
	unsigned long nTimeEnd = get_time ();
	
	// print benchmark data
	printf ("Total time:                 %g millisconds\n", (nTimeEnd - nTimeStart) / 1000.0);
	printf ("Image size:                 %ld x %ld = %ld Pixels\n",
		(long) IMAGE_WIDTH, (long) IMAGE_HEIGHT, (long) (IMAGE_WIDTH * IMAGE_HEIGHT));
	printf ("Total number of iterations: %ld\n", nTotalIterationsCount);
	printf ("Avg. time per pixel:        %g microseconds\n", (nTimeEnd - nTimeStart) / (double) (IMAGE_WIDTH * IMAGE_HEIGHT));
	printf ("Avg. time per iteration:    %g microseconds\n", (nTimeEnd - nTimeStart) / (double) nTotalIterationsCount);
	printf ("Iterations/second:          %g\n", nTotalIterationsCount / (double) (nTimeEnd - nTimeStart) * 1e6);
	// assume there are 8 floating point operations per iteration
	printf ("MFlop/s:                    %g\n", nTotalIterationsCount * 8.0 / (double) (nTimeEnd - nTimeStart));
	
	png_write (pPng, "mandel.png");
	return 0;
}
