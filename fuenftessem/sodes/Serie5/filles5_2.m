

%task 1b:
%PlotBrownianStoPro();
%intervall = BrownianStoProMonteCarlo(1,10^5);

%task 2a:
%PlotBrownianStoPro_2a();
%intervall_2a = BrownianStoProMonteCarlo_2a(1,10^5);

%task 2b:
PlotBrownianStoPro_2b();
intervall_2b = BrownianStoProMonteCarlo_2b(1,10^5);


function X = BrownianStoPro(T,N)
    W =BrownianMotion(T,N); 
   % X = W+1;
    X = W + [0:1/N:T] + 1;
end

function PlotBrownianStoPro()
    hold on;
    T = 1;
    N = 10^3;
    
    for i= 1:10
        %task 1: 
        %Y = BrownianStoPro(T,N);
        %task 2:
        Y = exp(2*BrownianMotion(T,N));
        tempmesh = [0:T/N:T];
        plot ( tempmesh , Y );
    end
  
end

function y = BrownianStoProMonteCarlo(T,N)
    RV =  BrownianStoPro(T,N);
    mu = sum (exp(RV))/N;
   
    sig = abs(sum(exp(RV) - mu)/N);
    y = [mu - 2*sig , mu + 2*sig ];
    
end 

function W = BrownianMotion(T,N) 
     
    W = cumsum( [ 0,randn(1,N)]*sqrt (T/N) ) ;
    
end
% exact value of mu should be exp(1/2*sig*t)/N

% seems totally wrong :( 


%Start of Exercise 2:

function PlotBrownianStoPro_2a()
    hold on;
    T = 1;
    N = 10^3;
    
    for i= 1:10
         
        Y = exp(2*BrownianMotion(T,N));
        tempmesh = [0:T/N:T];
        plot ( tempmesh , Y );
    end
  
end

function y = BrownianStoProMonteCarlo_2a(T,N)
    RV =  BrownianMotion(T,N);
    mu = sum (exp(2*RV))/N;
   
    sig = abs(sum(exp(2*RV) - mu)/N);
    y = [mu - 2*sig , mu + 2*sig ];
    
end 



function PlotBrownianStoPro_2b()
    hold on;
    T = 1;
    N = 10^3;
    
    for i= 1:10
       
        
        % not really sure what i am doing here ...
        W = cumsum(2.* round( rand(N+1,1)) - 1);
        Y = (exp(2*W));
        tempmesh = [0:T/N:T];
        plot ( tempmesh , Y );
    end
  
end


function z = BrownianStoProMonteCarlo_2b(T,N)
    RV = cumsum (2.* round( rand(N+1,1)) - 1);
    mu = (exp(2*RV))/N;
   
    sig = abs(sum(exp(2*RV) - mu)/N);
    z = [mu - 2*sig , mu + 2*sig ]
    
end 






