PlotBrownianMotion()


function W = BrownianMotion(T,N) 
     
    W = cumsum( [ 0,randn(1,N)]*sqrt (T/N) ) ;
    
end
function PlotBrownianMotion()
    hold on;
    T = 1;
    N = 1000;

    
    for i= 1:5
        W = BrownianMotion(T,N);
        tempmesh = [0:T/N:T];
        plot ( tempmesh , W, "b-" );
    end
  
end
  