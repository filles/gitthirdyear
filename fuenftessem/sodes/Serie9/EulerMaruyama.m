function Y = EulerMaruyama(T, m, xi, mu, sigma, W)
N = .... ;
h = .... ;
M = .... ;
Y = .... ;
for i = 1:N
    dW = W(i+1, :)-W(i, :);
    dW1 = reshape(dW, [M, m])';
    dWnew = dW1(:);
    I = repmat(1:M, m, 1);
    I = I(:);
    J = 1:M*m;
    dW_sparse = (sparse(I, J, dWnew))';
    Y = ....;
end
end
