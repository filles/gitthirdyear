#include <boost/config.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/directed_graph.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <boost/graph/read_dimacs.hpp>
#include <boost/graph/graph_utility.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <typeinfo>

#include <boost/type_index.hpp>

#include <vector>
#include <queue>


typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;

typedef boost::adjacency_list<boost::listS, boost::vecS, boost::directedS, boost::no_property,
		boost::property<boost::edge_capacity_t, long, 
		boost::property<boost::edge_residual_capacity_t, long,
		boost::property<boost::edge_reverse_t, Traits::edge_descriptor>>>> Graph;

typedef boost::property_map<Graph, boost::edge_capacity_t>::type EdgeCapacityMap;
typedef boost::property_map<Graph, boost::edge_residual_capacity_t>::type ResidualCapacityMap;
typedef boost::property_map<Graph, boost::edge_reverse_t>::type ReverseEdgeMap;

typedef boost::graph_traits<Graph>::vertex_descriptor Vertex;
typedef boost::graph_traits<Graph>::edge_descriptor Edge;

void addEdge(int from, int to, long capacity, EdgeCapacityMap &capacitymap,
	     ReverseEdgeMap &revedgemap, Graph& g)
{
	Edge e, rev_e;
	bool success;
	boost::tie(e, success) = boost::add_edge(from, to, g);
	boost::tie(rev_e, success) = boost::add_edge(to, from, g);
	capacitymap[e] = capacity;
	capacitymap[rev_e] = 0;
	revedgemap[e] = rev_e;
	revedgemap[rev_e] = e;
}

// 1. Ansatz
//adding a function that extracts the min cut from the graph 
//PRE:: add a Graph with max flow calculated
//POST: returns a graph  with the indicies of the nodes in the S part of the min cut
//std::vector<Graph> extractmincut(Graph& g, Vertex s, Vertex t, unsigned flow){
//	std::vector<Edge>
//}

//2. Ansatz von der Folie
std::vector<bool> extractmincut (Graph & g, Vertex s){
	 ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, g);
	 std::vector<bool> vis (boost::num_vertices(g),false);
	 std::queue<int> Q;
	 vis[s] = true;
	 Q.push(s);
	 while(!Q.empty()){
	 	const int u = Q.front();
		Q.pop();
		boost::graph_traits<Graph>::out_edge_iterator  ebeg, eend;
		for(boost::tie(ebeg, eend) = boost::out_edges(u,g); ebeg != eend; ++ebeg) {
	 		const int v = boost::target(*ebeg,g);
			
			if(rescapacitymap[*ebeg] == 0 || vis[v]) continue;
			vis[v] = true;
			Q.push(v);
		}
	 }
	return vis;

}



void print(Graph& g, EdgeCapacityMap& capacitymap)
{
	boost::graph_traits<Graph>::vertex_iterator vi, vi_end;
	typename boost::graph_traits < Graph >::out_edge_iterator ei, ei_end;
	for(boost::tie(vi, vi_end) = boost::vertices(g); vi != vi_end; ++vi)
	{
		std::cout << *vi << ": ";
		for(tie(ei, ei_end) = out_edges(*vi, g); ei != ei_end; ++ei)
			if(capacitymap(*ei) > 0)
				std::cout << *ei << " ";
		std::cout << std::endl;	
	}
}


void test_flow()
{
	Graph g(4);
	EdgeCapacityMap capacitymap = boost::get(boost::edge_capacity, g);
	ReverseEdgeMap revedgemap = boost::get(boost::edge_reverse, g);
	ResidualCapacityMap rescapacitymap = boost::get(boost::edge_residual_capacity, g);
/*
	addEdge(0, 1, 1, capacitymap, revedgemap, g);
	addEdge(0, 3, 1, capacitymap, revedgemap, g);
	addEdge(2, 1, 1, capacitymap, revedgemap, g);
	addEdge(2, 3, 1, capacitymap, revedgemap, g);

	addEdge(0, 2, 1, capacitymap, revedgemap, g);
	addEdge(1, 3, 1, capacitymap, revedgemap, g);

	Vertex s = boost::add_vertex(g);
	Vertex t = boost::add_vertex(g);

	addEdge(s, 0, 2, capacitymap, revedgemap, g);
	addEdge(s, 2, 1, capacitymap, revedgemap, g);

	addEdge(1, t, 2, capacitymap, revedgemap, g);
	addEdge(3, t, 1, capacitymap, revedgemap, g);
*/
	Vertex s = boost::add_vertex(g);
	addEdge(0, 1, 5, capacitymap, revedgemap, g);
	addEdge(1, 2, 3, capacitymap, revedgemap, g);
	addEdge(1, 3, 2, capacitymap, revedgemap, g);
	addEdge(2, 4, 2, capacitymap, revedgemap, g);
	addEdge(2, 3, 1, capacitymap, revedgemap, g);
	addEdge(3, 4, 2, capacitymap, revedgemap, g);
	Vertex t = boost::add_vertex(g);
	addEdge(4, t, 4, capacitymap, revedgemap, g);
	
	auto f = g;
	long flow1 = push_relabel_max_flow(g, s, t);
	long flow2 = edmonds_karp_max_flow(f, s, t);

	std::cout << "flow1 is " << flow1 << "\n";
	std::cout << "flow2 is " << flow2 << "\n";
	
	auto cut = extractmincut(g,s);
	auto cut2 = extractmincut(f,s);

	for( int i = 0; i < boost::num_vertices(g); ++i ){
		std::cout << "vertex : " << i  <<" is in s cut " << cut[i] << std::endl;
	}
	for( int i = 0; i < boost::num_vertices(g); ++i ){
		std::cout << "vertex : " << i  <<" is in s cut " << cut2[i] << std::endl;
	}

	long alpha = 1;
	// now iterate and add
	
	print(g, capacitymap);
	Vertex a = boost::add_vertex(g);
	boost::graph_traits<Graph>::vertex_iterator vi, vi_end;
	
	for(boost::tie(vi, vi_end) = boost::vertices(g); vi != vi_end; ++vi)
	{
		if(*vi == a)
		{
			// type checking block
			//std::cout << typeid(*vi).name() << "\n";
			//std::cout << type_name<decltype(a)>() << "\n";
			//std::cout << boost::typeindex::type_id<decltype(a)>().pretty_name() << "\n";
			//std::cout << boost::typeindex::type_id<decltype(vi)>().pretty_name() << "\n";
			//std::cout << boost::typeindex::type_id<decltype(*vi)>().pretty_name() << "\n";
			continue;
		}
		addEdge(int(a), int(*vi), alpha, capacitymap, revedgemap, g);
	}
	std::cout << std::endl;
	print(g, capacitymap);
}


int main(int argc, char** argv)
{
	test_flow();
}
