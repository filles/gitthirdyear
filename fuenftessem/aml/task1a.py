from sklearn.linear_model import Ridge
from sklearn.metrics import  mean_squared_error
from sklearn.model_selection import KFold
import pandas as pd
import numpy as np
import csv


data = pd.read_csv("train.csv")

data.pop('Id') 

split = KFold(n_splits=10)

lambdas = [0.1, 1.0, 10.,100.,1000.]

Errors = []

for lam in lambdas: 

		model = Ridge(alpha = lam)
		RMSE = 0.

		for train_index, test_index in split.split(data):
			train = data.filter(train_index, axis = 0)
			y_train = train.pop('y')
			model.fit(train , y_train)

			test = data.filter(test_index , axis = 0) 
			y_test = test.pop('y')
			predictions = model.predict(test)
			RMSE += mean_squared_error(predictions, y_test )**0.5/10.
	
		Errors.append(RMSE)

		
output = open("res.csv", 'w')
for i in range(5):
	output.write(str(Errors[i]))
	output.write('\n')

output.close()
