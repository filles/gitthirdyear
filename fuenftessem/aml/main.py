import pandas as pd
import numpy as np 
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import model_from_json
from tensorflow.keras import optimizers
from tensorflow.keras import losses
from sklearn.preprocessing import normalize
from sklearn.preprocessing import StandardScaler
from sklearn.utils import class_weight

#transforming the data to use it in our models
train = pd.read_csv("X_train.csv")
test = pd.read_csv("X_test.csv")
sample = pd.read_csv("sample.csv")
y_tra = pd.read_csv("y_train.csv")
print(sample)
Id = sample.pop("id")
#
##taking only the values, not the indices etc.
#x_train = train.values
#x_test = test.values
#
##transforming into the right datatype
##y_int = y_tra.values #vector
##y_train = to_categorical(y_int)#same but as matrix
##print(y_int)
##print(y_train)
#
##json_file = open("model02.json", "r")
##loaded_model_json = json_file.read()
##json_file.close()
##model = model_from_json(loaded_model_json)
##model.load_weights("weights02.h5")
##
#
#scaler = StandardScaler()
#x_train = scaler.fit_transform(x_train)
#x_test  = scaler.fit_transform(x_test )
#
##normalize(x_train, axis = 1) 
##normalize(x_test, axis = 1)
#
##batchsize=32
#
##the model
model = LinearModel()
opt = tf.keras.optimizers.Adam()
loss_fn = tf.keras.losses.MeanSquaredError()
with tf.GradientTape() as tape:
  output = model(sparse_input)
  loss = tf.reduce_mean(loss_fn(target, output))
grads = tape.gradient(loss, model.weights)
opt.apply_gradients(zip(grads, model.weights))
#model = keras.Sequential([
#    keras.layers.Dense(775,  activation='elu'),
#    keras.layers.Dropout(0.25),
#    keras.layers.Dense(60 ,  activation='elu'),
#    #keras.layers.Dense(60,  activation='hard_sigmoid'),
##   #keras.layers.Dense(60, kernel_regularizer=tf.keras.regularizers.l2(0.01)),
##    keras.layers.Dropout(0.2),
##    keras.layers.Dense(30,  activation='sigmoid'),
#    keras.layers.Dense(30,  activation='elu'),
#    keras.layers.Dropout(0.3),
#    keras.layers.Dense(15,  activation=tf.nn.relu),
#    #keras.layers.Dropout(0.2),
#    keras.layers.Dense(775, activation=tf.nn.softmax)#must be 5 because of the 5 different discrete values given in the task description
#])
#
#
##erster ansatz
#model.compile(optimizer= 'adam', loss= 'hinge', metrics=['accuracy'])
#
#
##meine Versuche
#sgd = optimizers.SGD(lr=0.01, decay=1e-4, momentum=0.9, nesterov=True)
##sgd = optimizers.SGD(lr=0.01, clipnorm=1.)
##sgd = optimizers.SGD(lr=0.01, clipvalue=0.5)
## 1. SChritt :
## adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.00, amsgrad=True)
## 2.Schritt
## adam = optimizers.Adam(lr=0.001, beta_1=0.5, beta_2=0.999, epsilon=None, decay=0.001, amsgrad=False)
#
#
#
#adam = optimizers.Adam(lr=0.0001, beta_1=0.5, beta_2=0.999, epsilon=None, decay=0.001, amsgrad=True )
#addelta = optimizers.Adadelta(lr=0.001)
#model.compile(optimizer=adam,
#              loss=losses.categorical_crossentropy,
#			  #loss= tf.nn.sigmoid_crossentropy,
#			  #loss= losses.categorical_hinge,
#              metrics=['accuracy'])
#
#class_weights = class_weight.compute_class_weight('balanced',
#                                                 np.unique(y_train),
#                                                 y_train)
#

#evaluating
model.fit(x_train, y_train, epochs = 1, steps_per_epoch = 1000, callbacks=[keras.callbacks.EarlyStopping(patience= 5, monitor='loss')])#steps per epoch = (no. samples/batchsize)

#  steps_per_epoch = 500 ,
# class_weight='auto',
#predicting
predictions = model.predict(x_test)
#print(predictions)
predict_class = np.argmax(predictions, axis=1)
predict_class = predict_class.tolist()
#print(predict_class)


###################
#model_json = model.to_json()
#with open("model02.json","w") as json_file:
#	json_file.write(model_json)
#model.save_weights('weights02.h5')
##############

#creating our output
names = ["Id", "y"]
Id = pd.DataFrame(Id)
result = pd.DataFrame(predict_class)

sol = Id.join(result)
sol.columns = names

sol.to_csv("losungfinal.csv", index =False)
