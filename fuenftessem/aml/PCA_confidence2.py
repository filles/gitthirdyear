import pandas as pd
import h5py
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split, KFold

train_labeled = pd.read_csv("X_train.csv", "train")          #reading in data
#train_unlabeled = pd.read_hdf("train_unlabeled.h5", "train")
test = pd.read_csv("X_test.csv", "test")
y_test = pd.read_csv("y_train.csv", "y_test")

x_np_labeled = train_labeled.to_numpy()                            #data to np array
y_np_labeled = y_test.to_numpy()
np_test = test.to_numpy()

#print(np_labeled.shape)
#x_np_labeled = np_labeled[:,1:140]
#y_np_labeled = np_labeled[:,0]
#print(np_test.shape)

#std_scale_labeled = preprocessing.StandardScaler().fit(x_np_labeled) # labeled training data normalization
#x_np_labeled = std_scale_labeled.transform(x_np_labeled)

#std_scale_unlabeled = preprocessing.StandardScaler().fit(np_unlabeled) # unlabeled training data normalization
#np_unlabeled = std_scale_unlabeled.transform(np_unlabeled)

#std_scale_test = preprocessing.StandardScaler().fit(np_test) # testing data normalization
#np_test = std_scale_test.transform(np_test)

#print(x_np_labeled.shape)
#print(np_unlabeled.shape)

#x_total = np.concatenate((x_np_labeled,np_unlabeled),axis=0)  # total data points (PCA)
#std_scale_total = preprocessing.StandardScaler().fit(x_total) # normalization
#x_total = std_scale_total.transform(x_total)
#print(x_total.shape)

############################## optically (or numerically) determine k ##################################
#
#pca = PCA().fit(x_total)
#plt.figure()
#plt.plot(np.cumsum(pca.explained_variance_ratio_))
#plt.show()
#
################### 60 <= k <= 80 preserves around .999% empericial covariance ################
#
#k = 70
#
######################## apply PCA (k-dim transormation) ##############################################
#
#pca = PCA(n_components=k)
#pca.fit(x_total)
#
#x_np_labeled = pca.transform(x_np_labeled)
#np_unlabeled = pca.transform(np_unlabeled)
#np_test = pca.transform(np_test)
#print(np_test.shape)
#
###################################### function for ranking confidence #####################################################
#
#def rank_confidence(y_unlabeled_cat, x_unlabeled): # y_unlabeled_cat: predicted by model ; x_unlabeled: input to model
#
#    y_unlabeled = np.argmax(y_unlabeled_cat,axis=1) # class label vector format
#    y_confidence_score = np.zeros(0)                # label highest confidence score
#
#    for i in range(0,y_unlabeled.size): # == y_unlabeled_cat.size
#        y_confidence_score = np.append(y_confidence_score,y_unlabeled_cat[i,np.argmax(y_unlabeled_cat[i,:],axis=0)])
#
#    confidence_score = np.column_stack((np.column_stack((y_confidence_score,y_unlabeled)),x_unlabeled))
#    confidence_score = confidence_score[confidence_score[:,0].argsort()[::-1]]
#
#    return confidence_score # returns np array with 1. column: confidence ; 2. column: class predicted ; 3-141 column: x-vals
#
#    ################################## sequential model training & prediction #############################

    import tensorflow as tf
    import tensorflow.keras as keras
    from tensorflow.keras.models import Sequential
    from tensorflow.keras import optimizers, models
    from tensorflow.keras.layers import Dense, Dropout, Activation, BatchNormalization

    ##################################### model settings ##########################################################

    keras.backend.clear_session()

    model = Sequential()   # our neural net
    # Dense(256) is a fully-connected layer with 256 hidden units.
    # in the first layer, you must specify the expected input data shape:
    # here, 120-dimensional vectors.
    model.add(Dense(240, activation='relu', input_dim=k))    #add layers
    model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
    model.add(Dropout(0.4))

    model.add(Dense(180, activation='relu'))
    model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
    model.add(Dropout(0.3))

    model.add(Dense(90, activation='relu'))
    model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
    model.add(Dropout(0.4))

    model.add(Dense(60, activation='relu'))
    model.add(BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001, center=True, scale=True, beta_initializer='zeros', gamma_initializer='ones', moving_mean_initializer='zeros', moving_variance_initializer='ones', beta_regularizer=None, gamma_regularizer=None, beta_constraint=None, gamma_constraint=None))
    model.add(Dropout(0.4))

    model.add(Dense(30, activation='relu'))
    model.add(Dropout(0.0))

    model.add(Dense(10, activation='softmax'))

    opt = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

    ######################################### first run ##########################################################

    y_np_labeled_cat = keras.utils.to_categorical(y_np_labeled,num_classes=10)

    model.fit(x_np_labeled, y_np_labeled_cat, batch_size=64, epochs=100)

    ##################################### confidence selection & data augmentation ###################################

x_unlabeled_rest = np_unlabeled   # new variable names in loop
x_total_training = x_np_labeled
y_total_training = y_np_labeled

y_unlabeled_cat = model.predict(x_unlabeled_rest)

perc = 0.95 # minimum % confidence in prediction for first iteration
confident = True

while True:

    unlabeled_confidence = rank_confidence(y_unlabeled_cat,x_unlabeled_rest) # get ranked confidence of predictions

    high_confidence = unlabeled_confidence[unlabeled_confidence[:,0]>=perc] # predictions model is confident in
    low_confidence = unlabeled_confidence[unlabeled_confidence[:,0]<perc]   # not confident

    x_labeled_new = high_confidence[:,2:141]  # new labeled data
    y_labeled_new = high_confidence[:,1]
    x_unlabeled_rest = low_confidence[:,2:141] # "new" unlabeled data

    x_total_training = np.concatenate((x_total_training,x_labeled_new),axis=0)  # add new traning data to old training data
    y_total_training = np.concatenate((y_total_training,y_labeled_new),axis=0)  # (top perc % confident predictions)
    y_total_training_cat = keras.utils.to_categorical(y_total_training,num_classes=10)

    if np.size(high_confidence,axis=0) < 10:
        perc -= 0.5

    if perc < 0.75:
        break

    model.fit(x_total_training, y_total_training_cat, batch_size=64, epochs=100)
    y_unlabeled_cat = model.predict(x_unlabeled_rest)



test_pred_cat = model.predict(np_test)
test_pred = np.argmax(test_pred_cat,axis=1)

################# output (! predicted output is test_pred (class vector format) !) ######################################

header = ["Id", "y"]
indices = test.index.to_numpy()
result = np.ones((8000,2),dtype=int)
result[:,0] = indices
result[:,1] = test_pred
result_df = pd.DataFrame(result)
result_df.columns = header
print(result_df)
result_df.to_csv('pca_confidence_2.csv', sep=',', index = False)
