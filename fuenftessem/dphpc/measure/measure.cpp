 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
#include <mpi.h>
#include <liblsb.h>
#include <stdlib.h>
#include <time.h>

#define N 1024
#define RUNS 10

int main(int argc, char *argv[]){
    int i, j, rank;
    int buffer[N];
    
    srand(time(NULL));
    for (int i=0; i<N; i++) buffer[i]=rand();

    MPI_Init(&argc, &argv);
    LSB_Init("test_bcast", 0);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /* Output the info (i.e., rank, runs) in the results file */
    LSB_Set_Rparam_int("rank", rank);
    LSB_Set_Rparam_int("runs", RUNS);

    for (i=1; i<=N; i*=2){

        for (j=0; j<RUNS; j++){
            /* Reset the counters */
            LSB_Res();

            /* Perform the operation */
            MPI_Bcast(buffer, i, MPI_INT, 0, MPI_COMM_WORLD);

            /* Register the j-th measurement with the size i */        
            LSB_Rec(i);
        }
    }

    LSB_Finalize();
    MPI_Finalize();
    return 0;
}
