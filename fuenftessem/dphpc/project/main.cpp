#include "doubleEdgeGraph.hpp"
#include <string>
#include <fstream>




int main(int argc, char* argv[])
{
    Graph g;
    //directory in which the datasets are located (first argument)
    std::string test_dir; 
    int number_data_sets = argc > 1 ? argc - 2 : 1;

	//data set names arrar (all following arguments)
    std::string data_set_names[number_data_sets];
    
    if(argc == 1) 
    {
        //default case

        test_dir = "./data/"; 
        //data_set_names[0] = "paper_test.txt";
        data_set_names[0] = "default.txt";
        std::cout << "default directory = " << test_dir << std::endl;
	}
	else 
	{
		//case with arguments
		
        test_dir = argv[1];

        for(int i = 0; i < number_data_sets; ++i)
        {
            data_set_names[i] = argv[i+2];
        }

        std::cout << "data sets in directory " << test_dir << std::endl;
    }
    
    //ifstream object needed for read_dimacs_min_cut function
    std::ifstream graphFile; 
    for(int i = 0; i < number_data_sets; ++i)
    {
        graphFile.open(test_dir + data_set_names[i]);

        // error case in case file does not exist
        if(!graphFile)
        {
            std::cerr << "Unable to open file " << data_set_names[i] << std::endl;
            std::cerr << "correct usage = directory followed by projectfile names. (for example \" ./data/ graph.txt \" )" << std::endl;
            std::exit(1);
        }

        std::cout << "opened file " << data_set_names[i] << std::endl;

        EdgeCapacityMap capacity = boost::get(boost::edge_capacity, g);
        ReverseEdgeMap rev = boost::get(boost::edge_reverse, g);
  		ResidualCapacityMap res =	get(boost::edge_residual_capacity, g);
        boost::read_dimacs_min_cut(g, capacity, rev, graphFile);


        ///////////////////////////DO STUFF WITH GRAPH//////////////////////////////
		int alpha = 1;

		// if called throws algo.is_flow excaption at push_relabel
		// error condiditon in is_flow_error.cpp 

		//Graph h = make_double_edge_graph(g, capacity, rev);
        //EdgeCapacityMap cap_h = boost::get(boost::edge_capacity, h);
        //ReverseEdgeMap rev_h = boost::get(boost::edge_reverse, h);
		//CalcMinCutTree( g, alpha, cap_h, rev_h);
		//make_double_edge_graph(g, capacity, rev);
		
		ungraph p = CalcMinCutTree( g, alpha, capacity, rev);
		

		//print(g);
    }

    return 0;
}


ungraph CalcMinCutTree(Graph& g_in, int aplha, EdgeCapacityMap& capacity,
	ReverseEdgeMap& rev  )
	{

	int n = boost::num_vertices(g_in);
	std::cout << " num of nodes " << n << std::endl;

	//build gmhu tree
	ungraph gmhu;
	for (int i = 1; i<n ;  ++i){ 
		boost::add_edge(0,i,1,gmhu);
	}

	// vector for extracting the mincut	
	std::vector<int> vis;
	int flow ;
	Vertex s;
	Vertex t;


	//----------------------------------------------//
	// assuming 0 is the artificial node added in g //
	//----------------------------------------------//
	//if  artificial is last node change loop from 0 to n - 2



	
	//to be improved
	//hier wird der Tempgraph gebaut 
	// Trying to copy Graph with property maps so that algo works
	// -------------- Versuch #1 --------------------
   	//Graph g ;	
   	//EdgeCapacityMap cap_g= boost::get(boost::edge_capacity, g);
   	//ReverseEdgeMap rev_g = boost::get(boost::edge_reverse, g);
   	//ResidualCapacityMap res_g =	get(boost::edge_residual_capacity, g);
   	//boost::read_dimacs_min_cut(g, cap_g, rev_g, graphFile);
	// -------------- Versuch #2 --------------------
    //Graph g = g_in;	
   	//cap_g= capacity ;
   	//rev_g = rev;
	// -------------- Versuch #3 --------------------
    //Graph g;	
    //EdgeCapacityMap cap_g= boost::get(boost::edge_capacity, g);
    //ReverseEdgeMap rev_g = boost::get(boost::edge_reverse, g);
    //ResidualCapacityMap res_g =	get(boost::edge_residual_capacity, g);
	//boost::copy_graph(g_in,g);
	// -------------- Versuch #4 --------------------
   	std::ifstream graphFile; 
   	//graphFile.open("./data/paper_test.txt");
   	graphFile.open("./data/default.txt");
	Graph g ;	
	EdgeCapacityMap cap_g= boost::get(boost::edge_capacity, g);
	ReverseEdgeMap rev_g = boost::get(boost::edge_reverse, g);
	ResidualCapacityMap res_g =	get(boost::edge_residual_capacity, g);


	// some tests that show wheter the copying works 
	//print(g,cap_g);
	//is_flow (g,cap_g,rev_g,res_g);
	//print(g_in,capacity);
	

	for (int i = 1; i < n ;  ++i){ 
	
	// -------------- Versuch #4 continued-----------
			// resets the ifstream in each iteration
			graphFile.clear();
			graphFile.seekg(0, std::ios::beg);
			boost::read_dimacs_min_cut(g, cap_g, rev_g, graphFile);
	// -------------- Versuch #4 end----------------

		//unsure here
		s = boost::vertex(i,g);
		t = boost::vertex(neighbour(gmhu,i),g);
		

		//flow = boost::push_relabel_max_flow(g, s, t);
		//, 
		boost::property_map<Graph, boost::vertex_index_t>::type idx_g =
			get(boost::vertex_index_t(), g);
		flow = boost::push_relabel_max_flow(g, s, t, cap_g, res_g, rev_g,idx_g)   ;

		std::cout << "The max flow between " << s << " and " << t << " is " << flow << std::endl;

		//res_g =	get(boost::edge_residual_capacity, g);
		vis = isinCut(g, s,res_g);
	
		//test if isinCut returns 1 for all the verticies that are in the cut of s
		//for( int i = 0; i < n ; ++i){
	 	//	std::cout << vis[i]<< std::endl;
		//}
		


		// add value of mincut to the  s-t edge of gmhu tree
		// workaround on the fly should be improved
		boost::remove_edge(t,s,gmhu);
		boost::add_edge(t,s,flow,gmhu);
		//boost::put(boost::edge_weight_t(),gmhu,boost::edge(s,t,gmhu),flow);

		// check nodes that have higher index than s
		// if they are in the s side of the cut
		// if yes -> anhängen an s 
		for(int j = s+1 ; j < n; ++j){
		
				
			//std::cout <<(vis[j]) << std::endl ;
				if(vis[j] == 1 && neighbour(gmhu,j) == t){
				//if(Q.end() !=	std::find(Q.begin(),Q.end(),i) ){
					boost::remove_edge(j,neighbour(gmhu,j),gmhu);
					std::cout << "hänge " << j << " an " << s << std::endl;
					boost::add_edge(s,j,1,gmhu);
				}
			}
		}
		

	return gmhu;	
	}
	 
std::vector<int> isinCut(Graph& g, Vertex s,ResidualCapacityMap& rescapacitymap){
	ResidualCapacityMap res_map = boost::get(boost::edge_residual_capacity, g);
	boost::graph_traits<Graph>::vertex_iterator vi, vi_end;
	boost::tie(vi, vi_end) = boost::vertices(g); 
	size_t N = vi_end-vi;


	std::vector<int> visited(N, false);
	std::queue<int> Q;
	visited[int(s)] = true;
	Q.push(int(s));
	while(not Q.empty())
	{
		const int u = Q.front();
		Q.pop();
		typename boost::graph_traits < Graph >::out_edge_iterator ei, ei_end;
		for(boost::tie(ei, ei_end) = boost::out_edges(u, g); ei != ei_end; ++ei)
		{
			//const int v = boost::source(*ei, g);
			const int v = boost::target(*ei, g);
			//if(rescapacitymap[*ei] == 0 || visited[v]) continue;
			//std::cout << " checking resflow from: "<< u << " to " << v << " it is : " <<
			//res_map[*ei]  << std::endl;	
			if(res_map[*ei] == 0 || visited[v]) continue;
			visited[v] = true;
			Q.push(v);
		}
	}
	return visited;
}

