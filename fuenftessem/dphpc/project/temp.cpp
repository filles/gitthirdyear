//Kommentare: 
//festgelegt: input graph darf node 0 nichtenthalten
//
//
#include <iostream>
#include <boost/thread.hpp>
#include <boost/graph/adjacency_list.hpp>

using namespace boost;

	double alpha= 1.;


	typedef property<edge_weight_t, int> EdgeWeightProperty;
	template <class OutEdgeListS = vecS,
	// a Sequence or an AssociativeContainer class VertexListS = vecS,
	// a Sequence or a RandomAccessContainer class DirectedS = directedS,
	class VertexProperty = no_property,
	class EdgeProperty = EdgeWeightProperty,
	class GraphProperty = no_property,
	class EdgeListS = listS>
	class adjacency_list {  };
	typedef boost::adjacency_list<listS, vecS, undirectedS,no_property, property<edge_weight_t,double> > mygraph;


int main(){
    std::cout << "Hello" << std::endl;
    mygraph g;
    
    //artificial sink 0
    //add_edge (0, 1, g);
    //add_edge (0, 2, g);
    //add_edge (0, 3, g);
    //add_edge (0, 4, g);
    //add_edge (0, 5, g);
    //add_edge (0, 6, g);
    //add_edge (0, 7, g);
    //add_edge (0, 8, g);
    //add_edge (0, 9, g);
    //add_edge (0, 10, g);
    
    //1. cluster
    add_edge (1, 2, g);
    add_edge (1, 3, g);
    //add_edge (2, 3, g);
    
    //2. cluster
    add_edge (4, 5, g);
    //add_edge (4, 6, g);
    add_edge (4, 7, g);
    add_edge (5, 6, g);
    //add_edge (5, 7, g);
    add_edge (6, 7, g);
    
    //3. cluster
    add_edge (8, 9, g);
    add_edge (8, 10, g);
    add_edge (9, 10, g);
    
    //Verbindungen
    add_edge (1, 4, g);
    add_edge (7, 8, g);
    
    
    mygraph::vertex_iterator vertexIt, vertexEnd;
    mygraph::adjacency_iterator neighbourIt, neighbourEnd;
    tie(vertexIt, vertexEnd) = vertices(g);
    for (; vertexIt != vertexEnd; ++vertexIt)
    {
        std::cout << *vertexIt << " is connected with ";
        tie(neighbourIt, neighbourEnd) = adjacent_vertices(*vertexIt, g);
        for (; neighbourIt != neighbourEnd; ++neighbourIt)
            std::cout << *neighbourIt << " ";
        std::cout << "\n";
    }


    return 0;
}



mygraph CutClusterBasic(mygraph g, double alpha){
	//using namespace boost 
	mygraph::vertex_iterator vertexIt, vertexEnd;
 	tie(vertexIt, vertexEnd) = vertices(g);

	//add artificial Sink 0 connected to all nodes with edgevalue alpha
	for (int i = 0; vertexIt != vertexEnd; ++vertexIt, ++i){ 
		add_edge(0,i,alpha,g);
	}	

	mygraph tempgraph = g;	

	CalcMinCutTree(tempgraph);

	remove_vertex(0,g);

	return g;

}


mygraph CalcMinCutTree (mygraph g){
//tb implemented
}
