#include <timer.hpp>
#include "gomory_hu_helper.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <boost/graph/push_relabel_max_flow.hpp>


UndirectedGraph cut_cluster_basic(Graph& g);

std::vector<bool> is_in_cut(Graph& g, Vertex s);

void get_connected_components(UndirectedGraph& p);

void run_default(Graph& g, UndirectedGraph &p);

void run_time_test(Graph& g, UndirectedGraph &p);

int main(int argc, char* argv[])
{
    Graph g;
    // directory in which the datasets are located (first argument)
    std::string test_dir; 
	capacity_t alpha = 1.;
    int number_data_sets = argc > 1 ? argc - 3 : 1;

	//data set names vector (all following arguments)
	std::vector<std::string> data_set_names(number_data_sets);
    
    if(argc == 1) 
    {
        // default case

        test_dir = "./../data/"; 
        data_set_names[0] = "default.txt";
        std::cout << "default directory = " << test_dir << std::endl;
	}
	else 
	{
		// case with arguments
		
        test_dir = argv[1];

    //    for(int i = 0; i < number_data_sets; ++i)
     //   {
            data_set_names[0] = argv[2];
			//char temp [1];
			//temp [0]= *argv[3];
			//std::cout << temp[0] << "is alpha " << std::endl;
			//alpha = static_cast<capacity_t>(*argv[3]);
			alpha = atof(argv[3]);	
      //  }

       //std::cout << "data sets in directory " << test_dir << std::endl;
	   //std::cout << "alpha is : " << alpha<<std::endl;
    }
    
    // ifstream object needed for read_dimacs_min_cut function
    //for(int i = 0; i < number_data_sets-1; ++i)
    //for(int i = 0; i < number_data_sets; ++i)
    //{
		int i = 0 ;
		std::filebuf buf;
        //buf.open(test_dir + data_set_names[i], std::ios::in);
        buf.open(test_dir + data_set_names[i], std::ios::in);
		std::istream graphFile(&buf);

        // error case in case file does not exist
        if(!graphFile)
        {
            std::cerr << "Unable to open file " << data_set_names[i] << std::endl;
            std::cerr << "correct usage = directory followed by projectfile names. (for example \" ./data/ graph.txt \" )" << std::endl;
            std::exit(1);
        }

        //std::cout << "opened file " << data_set_names[i] << std::endl;

        EdgeCapacityMap capacity = boost::get(boost::edge_capacity, g);
        ReverseEdgeMap rev = boost::get(boost::edge_reverse, g);

        boost::read_dimacs_min_cut(g, capacity, rev, graphFile);

		//make_double_edge_graph(g, capacity, rev);

		// clustering coefficient alpha controlls sensitivity toward clustering
//		capacity_t alpha = 1.;

		// order important! add_artifical sink adds undirected edges (edges in both directions)
		capacity_t alphas [] = { 0.001, 0.003, 0.008, 0.01, 0.03, 0.05, 0.08, 0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.75, 0.9, 1.0, 1.5, 2.0, 3.0, 5.0, 10.0, 100.0};
		//while(i < alphas.size()){
		while(i < 22){
		alpha = alphas[i];
		add_artificial_sink(g, capacity, rev, alpha);
		
		// declaring Gomory Hu tree
		UndirectedGraph p;
	//	Graph p;

        ///////////////////////////DO STUFF WITH GRAPH//////////////////////////////
		run_default(g, p);
		++i;
		}
		// run_time_test(g, p);
    //}

    return 0;
}

void run_default(Graph& g, UndirectedGraph &p)
{
	// declaring our timer
	Timer t;

	t.start();

	p = cut_cluster_basic(g);

	//stoping our timer
	t.stop();

	double time = t.duration();

	get_connected_components(p);

	//std::cout << "computed in " << time << " seconds." << std::endl;
}

void run_time_test(Graph& g, UndirectedGraph &p)
{
	// declaring our timer
	Timer t;
	double time = 0.;
	int N_iter = 1000;

	for(int i = 0; i < N_iter; ++i)
	{
		t.start();

		p = cut_cluster_basic(g);

		//stoping our timer
		t.stop();
		time += t.duration();
	}
	time /= static_cast<double> (N_iter);

	get_connected_components(p);

	std::cout << "computed in " << time << " seconds." << std::endl;
}

void get_connected_components(UndirectedGraph& p)
{
	unsigned int n = boost::num_vertices(p);

	// removing artificial sink
	boost::clear_vertex(n - 1, p);
	boost::remove_vertex(n - 1, p);

	// initializing component vector
	std::vector<int> comp(n - 1);
	
	//calculating connected components of tree
	boost::connected_components(p, &comp[0]);
	std::cout << std::endl;
	
	std::cout << std::endl;
	// output of components
	int temp = 0;
	for(unsigned int i = 0; i < n - 1; ++i)
	{
		//std::cout << "\t vertex \t" << i << " is in cluster \t" << comp[i] << std::endl;
		 if(comp[i] > temp)
		 	temp = comp[i];
	}
	std::cout << temp;
}

UndirectedGraph cut_cluster_basic(Graph& g)
{
	int n = boost::num_vertices(g);
	// std::cout << " num of nodes " << n << std::endl << std::endl;

	// initialize gmhu tree
	UndirectedGraph gmhu;
	for (int i = 0; i < n - 1; ++i)
	{ 
		// n-1 corresponds to artificial sink
		boost::add_edge(n - 1, i, 1, gmhu);
	}

	// vector for extracting the mincut	(visited vector)
	std::vector<bool> vis;
	capacity_t flow;
	Vertex s;
	Vertex t;

	// Program EQ page 146 paper 0219009 (Krabbenpaper)
	// starting Gusfield algorithm
	boost::property_map<Graph, boost::vertex_index_t>::type index_map = get(boost::vertex_index_t(), g);
	for(int j = 0; j < n - 1; ++j)
	{
		// set current vertex 
		s = boost::vertex(j, g);

		// set unique neighbour t := p[s]
		unsigned int b = find_unique_neighbour(gmhu, j);
		t = boost::vertex(b, g);

		// output the edge (s,t)
		 //std::cout << "\t (" << index_map(s) << " s " << j << ", " <<
		 //		index_map(t) << " t " << b << std::endl;
		
		////////////////////////// calculating maximum flow ///////////////////////////
		// same parameters as in boost documentation
		flow = boost::edmonds_karp_max_flow(g, s, t);
		//flow = push_relabel_max_flow(g, s, t);
		// std::cout << " flow = " << flow << std::endl;
		boost::remove_edge(j, index_map(t), gmhu);
		boost::add_edge(j, index_map(t), flow, gmhu);

		////////// get set X, i.e. the set of nodes on the s side of the cut///////////

		// maybe remove later and just create in function is_in_cut
		vis = is_in_cut(g, s);

		// reordering tree to become Gomory Hu tree
		for(int i = j + 1; i < n - 1; ++i)
		{
			if(vis[i] == true && find_unique_neighbour(gmhu, i) == index_map(t))
			{
				boost::remove_edge(boost::vertex(i, gmhu), t, gmhu);
				boost::add_edge(j, i, 1, gmhu);
			}
		}
	}

	// std::cout << "printing gmhu tree " << std::endl;
	// print(gmhu);
	// std::cout << std::endl << std::endl;

	return gmhu;	
}

std::vector<bool> is_in_cut(Graph& g, Vertex s)
{
	ResidualCapacityMap res = boost::get(boost::edge_residual_capacity, g);
	boost::graph_traits<Graph>::vertex_iterator vi, vi_end;
	boost::tie(vi, vi_end) = boost::vertices(g); 
	size_t N = vi_end - vi;

	std::vector<bool> visited(N, false);
	std::queue<int> Q;
	visited[int(s)] = true;
	Q.push(int(s));
	while(not Q.empty())
	{
		const int u = Q.front();
		Q.pop();
		typename boost::graph_traits < Graph >::out_edge_iterator ei, ei_end;
		for(boost::tie(ei, ei_end) = boost::out_edges(u, g); ei != ei_end; ++ei)
		{
			const int v = boost::target(*ei, g);

			if(res[*ei] == 0 || visited[v]) 
			{
				continue;
			}

			visited[v] = true;
			Q.push(v);
		}
	}

	return visited;
}

