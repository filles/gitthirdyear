/* 
   This code implements a parallel version of Gusfield's flow equivalent tree 
   algorithm using MPI.

   If results are reported, references should include:

   J. Cohen, L. A. Rodrigues, F. Silva, R. Carmo, A. Guedes, E. P. Duarte Jr., 
   "Parallel Implementations of Gusfield's Cut Tree Algorithm," 
   11th International Conference Algorithms and Architectures for Parallel Processing (ICA3PP), 
   pp. 258-269, Lecture Notes in Computer Science (LNCS) 7016, ISSN 0302-9743, Melbourne,
   Australia, 2011. 

   This code is derived from HIPR by IG Systems, Inc.
   HIPR implements a maximum flow - highest level push-relabel algorithm 
   http://www.igsystems.com/hipr/
   Commercial use requires a license.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <libgen.h>

#include "values.h"
#include "types.h"                     /* type definitions */
#include "parser_undirected.c"         /* parser */
#include "timer.c"                     /* timing routine */
#include <mpi.h>

/*
#define GLOB_UPDT_FREQ 0.5
*/
#define GLOB_UPDT_FREQ 0.5
#define ALPHA 6
#define BETA 12

#define WHITE 0
#define GREY 1
#define BLACK 2

/* global variables */

long   n;                    /* number of nodes */
long   m;                    /* number of arcs */
long   nm;                   /* n + ALPHA * m */
long   nMin;                 /* smallest node id */
node   *nodes;               /* array of nodes */
arc    *arcs;                /* array of arcs */
bucket *buckets;             /* array of buckets */
cType  *cap;                 /* array of capacities */
node   *source;              /* source node pointer */
node   *sink;                /* sink node pointer */
//node   **queue;              /* queue for BFS */
//node   **qHead, **qTail, **qLast;     /* queue pointers */
long   dMax;                 /* maximum label */
long   aMax;                 /* maximum actie node label */
long   aMin;                 /* minimum active node label */
double flow;                 /* flow value */
long pushCnt  = 0;           /* number of pushes */
long relabelCnt   = 0;       /* number of relabels */
long updateCnt    = 0;       /* number of updates */
long gapCnt   = 0;           /* number of gaps */
long gNodeCnt = 0;           /* number of nodes after gap */  
double t;                    /* for saving times */
node   *sentinelNode;        /* end of the node list marker */
arc *stopA;                  /* used in forAllArcs */
long workSinceUpdate=0;      /* the number of arc scans since last update */
float globUpdtFreq;          /* global update frequency */

typedef struct {
   int step;
   int source; 
   int sink; 
   double flow; //max flow
   char side;   //s or t
   int size;    //number of nodes in the cut
   double time_per_cut;
   int *nodes;
} reply;

int *buffer;
int bufsize = 0;

reply msg;

/* macros */

#define forAllNodes(i) for ( i = nodes; i != sentinelNode; i++ )
#define forAllArcs(i,a) for (a = i->first, stopA = (i+1)->first; a != stopA; a++)

#define nNode( i ) ( (i) - nodes + nMin )
#define Index( i ) ( (i) - nodes )
#define nArc( a )  ( ( a == NULL )? -1 : (a) - arcs )

#define min( a, b ) ( ( (a) < (b) ) ? a : b )

long i_dist;

#define aAdd(l,i)\
{\
  i->bNext = l->firstActive;\
  l->firstActive = i;\
  i_dist = i->d;\
  if (i_dist < aMin)\
    aMin = i_dist;\
  if (i_dist > aMax)\
    aMax = i_dist;\
  if (dMax < aMax)\
    dMax = aMax;\
}

/* i must be the first element */
#define aRemove(l,i)\
{\
  l->firstActive = i->bNext;\
}

node *i_next, *i_prev;
#define iAdd(l,i)\
{\
  i_next = l->firstInactive;\
  i->bNext = i_next;\
  i->bPrev = sentinelNode;\
  i_next->bPrev = i;\
  l->firstInactive = i;\
}

#define iDelete(l,i)\
{\
  i_next = i->bNext;\
  if (l->firstInactive == i) {\
    l->firstInactive = i_next;\
    i_next->bPrev = sentinelNode;\
  }\
  else {\
    i_prev = i->bPrev;\
    i_prev->bNext = i_next;\
    i_next->bPrev = i_prev;\
  }\
}

/* allocate datastructures, initialize related variables */

int allocDS( )

{

  nm = ALPHA * n + m;

  buckets = (bucket*) calloc ( n+2, sizeof (bucket) );
  if ( buckets == NULL ) return ( 1 );

  sentinelNode = nodes + n;
  sentinelNode->first = arcs + 2*m;
  return ( 0 );

} /* end of allocate */

void init( )
{
  node  *i;        /* current node */
  int overflowDetected;
  bucket *l;
  arc *a;
#ifdef EXCESS_TYPE_LONG
  double testExcess;
#endif
#ifndef OLD_INIT
  unsigned long delta;
#endif

  // initialize excesses

  forAllNodes(i) {
    i->excess = 0;
    i->current = i->first;
    forAllArcs(i, a)
      {
	a->resCap = cap[a-arcs];
	// a->rev->resCap = cap[a-arcs]; // for undirected graphs, did not work. jc
      }
  }

  for (l = buckets; l <= buckets + n-1; l++) {
    l -> firstActive   = sentinelNode;
    l -> firstInactive  = sentinelNode;
  }
    
  overflowDetected = 0;
#ifdef EXCESS_TYPE_LONG
  testExcess = 0;
  forAllArcs(source,a) {
    if (a->head != source) {
      testExcess += a->resCap;
    }
  }
  if (testExcess > MAXLONG) {
    printf("c WARNING: excess overflow. See README for details.\nc\n");
    overflowDetected = 1;
  }
#endif
#ifdef OLD_INIT
  source -> excess = MAXLONG;
#else

  if (overflowDetected) {
    source -> excess = MAXLONG;
  }
  else {
    source->excess = 0;
    forAllArcs(source,a) {
      if (a->head != source) {
	pushCnt ++;
	delta = a -> resCap;
	a -> resCap -= delta;
	(a -> rev) -> resCap += delta;
	a->head->excess += delta;
      }
    }
  }

  /*  setup labels and buckets */
  l = buckets + 1;
 
  aMax = 0;
  aMin = n;
    
  forAllNodes(i) {
    if (i == sink) {
      i->d = 0;
      iAdd(buckets,i);
      continue;
    }
    if ((i == source) && (!overflowDetected)) {
      i->d = n;
    }
    else
      i->d = 1;
    if (i->excess > 0) {
      /* put into active list */
      aAdd(l,i);
    }
    else { /* i -> excess == 0 */
      /* put into inactive list */
      if (i->d < n)
	iAdd(l,i);
    }
  }
  dMax = 1;
#endif

  //  dMax = n-1;
  //  flow = 0.0;

} /* end of init */

void checkMax()

{
  bucket *l;

  for (l = buckets + dMax + 1; l < buckets + n; l++) {
    assert(l->firstActive == sentinelNode);
    assert(l->firstInactive == sentinelNode);
  }
}

/* global update via backward breadth first search from the sink */

void globalUpdate ()

{

  node  *i, *j;       /* node pointers */
  arc   *a;           /* current arc pointers  */
  bucket *l, *jL;          /* bucket */
  long curDist, jD;
  long state;


  updateCnt ++;

  /* initialization */

  forAllNodes(i)
    i -> d = n;
  sink -> d = 0;

  dMax = n; // (jc - essa linha nao existia - resolveu o erro do dblcyc.1024 )
  for (l = buckets; l <= buckets + dMax; l++) {
    l -> firstActive   = sentinelNode;
    l -> firstInactive  = sentinelNode;
  }

  dMax = aMax = 0;
  aMin = n;

  /* breadth first search */

  // add sink to bucket zero

  iAdd(buckets, sink);
  for (curDist = 0; 1; curDist++) {

    state = 0;
    l = buckets + curDist;
    jD = curDist + 1;
    jL = l + 1;
    /*
    jL -> firstActive   = sentinelNode;
    jL -> firstInactive  = sentinelNode;
    */

    if ((l->firstActive == sentinelNode) && 
	(l->firstInactive == sentinelNode))
      break;

    while (1) {

      switch (state) {
      case 0: 
	i = l->firstInactive;
	state = 1;
	break;
      case 1:
	i = i->bNext;
	break;
      case 2:
	i = l->firstActive;
	state = 3;
	break;
      case 3:
	i = i->bNext;
	break;
      default: 
	assert(0);
	break;
      }

       if (i == sentinelNode) {
	if (state == 1) {
	  state = 2;
	  continue;
	}
	else {
	  assert(state == 3);
	  break;
	}
      }

      /* scanning arcs incident to node i */
      forAllArcs(i,a) {
	if (a->rev->resCap > 0 ) {
	  j = a->head;
	  if (j->d == n) {
	    j->d = jD;
	    j->current = j->first;
	    if (jD > dMax) dMax = jD;
	    
	    if (j->excess > 0) {
	      /* put into active list */
	      aAdd(jL,j);
	    }
	    else {
	      /* put into inactive list */
	      iAdd(jL,j);
	    }
	  }
	}
      } /* node i is scanned */ 
    }
  }

} /* end of global update */

/* second stage -- preflow to flow */
void stageTwo ( )
/*
   do dsf in the reverse flow graph from nodes with excess
   cancel cycles if found
   return excess flow in topological order
*/

/*
   i->d is used for dfs labels 
   i->bNext is used for topological order list
   buckets[i-nodes]->firstActive is used for DSF tree
*/

{
  node *i, *j, *tos, *bos, *restart, *r;
  arc *a;
  cType delta;

  /* deal with self-loops */
  forAllNodes(i) {
    forAllArcs(i,a)
      if ( a -> head == i ) {
	a -> resCap = cap[a - arcs];
      }
  }

  /* initialize */
  tos = bos = NULL;
  forAllNodes(i) {
    i -> d = WHITE;
    //    buckets[i-nodes].firstActive = NULL;
    buckets[i-nodes].firstActive = sentinelNode;
    i -> current = i -> first;
  }

  /* eliminate flow cycles, topologicaly order vertices */
  forAllNodes(i)
    if (( i -> d == WHITE ) && ( i -> excess > 0 ) &&
	( i != source ) && ( i != sink )) {
      r = i;
      r -> d = GREY;
      do {
	for ( ; i->current != (i+1)->first; i->current++) {
	  a = i -> current;
	  if (( cap[a - arcs] == 0 ) && ( a -> resCap > 0 )) { 
	    j = a -> head;
	    if ( j -> d == WHITE ) {
	      /* start scanning j */
	      j -> d = GREY;
	      buckets[j-nodes].firstActive = i;
	      i = j;
	      break;
	    }
	    else
	      if ( j -> d == GREY ) {
		/* find minimum flow on the cycle */
		delta = a -> resCap;
		while ( 1 ) {
		  delta = min ( delta, j -> current -> resCap );
		  if ( j == i )
		    break;
		  else
		    j = j -> current -> head;
		}

		/* remove delta flow units */
		j = i;
		while ( 1 ) {
		  a = j -> current;
		  a -> resCap -= delta;
		  a -> rev -> resCap += delta;
		  j = a -> head;
		  if ( j == i )
		    break;
		}
	  
		/* backup DFS to the first saturated arc */
		restart = i;
		for ( j = i -> current -> head; j != i; j = a -> head ) {
		  a = j -> current;
		  if (( j -> d == WHITE ) || ( a -> resCap == 0 )) {
		    j -> current -> head -> d = WHITE;
		    if ( j -> d != WHITE )
		      restart = j;
		  }
		}
	  
		if ( restart != i ) {
		  i = restart;
		  i->current++;
		  break;
		}
	      }
	  }
	}

	if (i->current == (i+1)->first) {
	  /* scan of i complete */
	  i -> d = BLACK;
	  if ( i != source ) {
	    if ( bos == NULL ) {
	      bos = i;
	      tos = i;
	    }
	    else {
	      i -> bNext = tos;
	      tos = i;
	    }
	  }

	  if ( i != r ) {
	    i = buckets[i-nodes].firstActive;
	    i->current++;
	  }
	  else
	    break;
	}
      } while ( 1 );
    }


  /* return excesses */
  /* note that sink is not on the stack */
  if ( bos != NULL ) {
    for ( i = tos; i != bos; i = i -> bNext ) {
      a = i -> first;
      while ( i -> excess > 0 ) {
	if (( cap[a - arcs] == 0 ) && ( a -> resCap > 0 )) {
	  if (a->resCap < i->excess)
	    delta = a->resCap;
	  else
	    delta = i->excess;
	  a -> resCap -= delta;
	  a -> rev -> resCap += delta;
	  i -> excess -= delta;
	  a -> head -> excess += delta;
	}
	a++;
      }
    }
    /* now do the bottom */
    i = bos;
    a = i -> first;
    while ( i -> excess > 0 ) {
      if (( cap[a - arcs] == 0 ) && ( a -> resCap > 0 )) {
	if (a->resCap < i->excess)
	  delta = a->resCap;
	else
	  delta = i->excess;
	a -> resCap -= delta;
	a -> rev -> resCap += delta;
	i -> excess -= delta;
	a -> head -> excess += delta;
      }
      a++;
    }
  }
}


/* gap relabeling */

int gap (emptyB)
     bucket *emptyB;

{

  bucket *l;
  node  *i; 
  long  r;           /* index of the bucket before l  */
  int   cc;          /* cc = 1 if no nodes with positive excess before
		      the gap */

  gapCnt ++;
  r = ( emptyB - buckets ) - 1;

  /* set labels of nodes beyond the gap to "infinity" */
  for ( l = emptyB + 1; l <= buckets + dMax; l ++ ) {
    /* this does nothing for high level selection 
    for (i = l -> firstActive; i != sentinelNode; i = i -> bNext) {
      i -> d = n;
      gNodeCnt++;
    }
    l -> firstActive = sentinelNode;
    */

    for ( i = l -> firstInactive; i != sentinelNode; i = i -> bNext ) {
      i -> d = n;
      gNodeCnt ++;
    }

    l -> firstInactive = sentinelNode;
  }

  cc = ( aMin > r ) ? 1 : 0;

  dMax = r;
  aMax = r;

  return ( cc );

}

/*--- relabelling node i */

long relabel (i)

node *i;   /* node to relabel */

{

  node  *j;
  long  minD;     /* minimum d of a node reachable from i */
  arc   *minA;    /* an arc which leads to the node with minimal d */
  arc   *a;

  assert(i->excess > 0);

  relabelCnt++;
  workSinceUpdate += BETA;

  i->d = minD = n;
  minA = NULL;

  /* find the minimum */
  forAllArcs(i,a) {
    workSinceUpdate++;
    if (a -> resCap > 0) {
      j = a -> head;
      if (j->d < minD) {
	minD = j->d;
	minA = a;
      }
    }
  }

  minD++;
      
  if (minD < n) {

    i->d = minD;
    i->current = minA;

    if (dMax < minD) dMax = minD;

  } /* end of minD < n */
      
  return ( minD );

} /* end of relabel */


/* discharge: push flow out of i until i becomes inactive */

void discharge (i)

node  *i;

{

  node  *j;                 /* sucsessor of i */
  long  jD;                 /* d of the next bucket */
  bucket *lj;               /* j's bucket */
  bucket *l;                /* i's bucket */
  arc   *a;                 /* current arc (i,j) */
  cType  delta;
  arc *stopA;

  assert(i->excess > 0);
  assert(i != sink);
  do {

    jD = i->d - 1;
    l = buckets + i->d;

    /* scanning arcs outgoing from  i  */
    for (a = i->current, stopA = (i+1)->first; a != stopA; a++) {
      if (a -> resCap > 0) {
	j = a -> head;

	if (j->d == jD) {
	  pushCnt ++;
	  if (a->resCap < i->excess)
	    delta = a->resCap;
	  else
	    delta = i->excess;
	  a->resCap -= delta;
	  a->rev->resCap += delta;

	  if (j != sink) {

	    lj = buckets + jD;

	    if (j->excess == 0) {
	      /* remove j from inactive list */
	      iDelete(lj,j);
	      /* add j to active list */
	      aAdd(lj,j);
	    }
	  }

	  j -> excess += delta;
	  i -> excess -= delta;
	  
	  if (i->excess == 0) break;

	} /* j belongs to the next bucket */
      } /* a  is not saturated */
    } /* end of scanning arcs from  i */

    if (a == stopA) {
      /* i must be relabeled */
      relabel (i);

      if (i->d == n) break;
      if ((l -> firstActive == sentinelNode) && 
	  (l -> firstInactive == sentinelNode)
	  )
	gap (l);

      if (i->d == n) break;
    }
    else {
      /* i no longer active */
      i->current = a;
      /* put i on inactive list */
      iAdd(l,i);
      break;
    }
  } while (1);
}


// go from higher to lower buckets, push flow
void wave() {

  node   *i;
  bucket  *l;

  for (l = buckets + aMax; l > buckets; l--) {
    for (i = l->firstActive; i != sentinelNode; i = l->firstActive) {
      aRemove(l,i);

      assert(i->excess > 0);
      discharge (i);

    }
  }
}


/* first stage  -- maximum preflow*/

void stageOne ( )

{

  node   *i;
  bucket  *l;             /* current bucket */


#if defined(INIT_UPDATE) || defined(OLD_INIT) || defined(WAVE_INIT)
  globalUpdate ();
#endif

  workSinceUpdate = 0;

#ifdef WAVE_INIT
  wave();
#endif  

  /* main loop */
  while ( aMax >= aMin ) {
    l = buckets + aMax;
    i = l->firstActive;

    if (i == sentinelNode)
      aMax--;
    else {
      aRemove(l,i);

      assert(i->excess > 0);
      discharge (i);

      if (aMax < aMin)
	break;

      /* is it time for global update? */
      if (workSinceUpdate * globUpdtFreq > nm) {
	globalUpdate ();
	workSinceUpdate = 0;
      }

    }
    
  } /* end of the main loop */
    
  flow = sink -> excess;

} 


void printReply(reply r, int rank) {
    int i;

    printf("t%d step=%d\n",rank, r.step);
    printf("t%d s=%d t=%d\n",rank,r.source, r.sink);
    printf("t%d flow = %lf\n", rank,r.flow);
    printf("t%d side=%c\n",rank,r.side);
    printf("t%d size = %d\n", rank,r.size);
    printf("t%d [",rank);
    for (i=0;i<r.size;i++)
	printf("%d ",r.nodes[i]);
    printf("]\n\n");
}

/*
int MPI_Pack(void* inbuf,int incount,MPI_Datatype datatype,
             void *outbuf,int outsize,int *position,MPI_Comm comm);
int MPI_Send(void* buf,int count,MPI_Datatype datatype,
    int dest,int tag,MPI_Comm comm);
*/

int sendAnswer() {
  int position = 0; //current position in the buffer

  MPI_Pack(&msg.step, 1, MPI_INT, buffer, bufsize, &position, MPI_COMM_WORLD);
  MPI_Pack(&msg.source, 1, MPI_INT, buffer, bufsize, &position, MPI_COMM_WORLD);
  MPI_Pack(&msg.sink, 1, MPI_INT, buffer, bufsize, &position, MPI_COMM_WORLD);
  MPI_Pack(&msg.flow, 1, MPI_DOUBLE, buffer, bufsize, &position, MPI_COMM_WORLD);
  MPI_Pack(&msg.side, 1, MPI_CHAR, buffer, bufsize, &position, MPI_COMM_WORLD);
  MPI_Pack(&msg.size, 1, MPI_INT, buffer, bufsize, &position, MPI_COMM_WORLD);
  MPI_Pack(&msg.time_per_cut, 1, MPI_DOUBLE, buffer, bufsize, &position, MPI_COMM_WORLD);  
  MPI_Pack(msg.nodes, msg.size, MPI_INT, buffer, bufsize, &position, MPI_COMM_WORLD);

  int to = 0; //always to master
  int tag = 1;
  int r = MPI_Send(buffer, position, MPI_PACKED, to, tag, MPI_COMM_WORLD);
//  printf("enviou...%d\n",r);
  return r;
}

int receiveAnswer() {
  int position = 0;
  int tag = 1;
  MPI_Status status;

  // receive the structure in the buffer
  MPI_Recv(buffer, bufsize, MPI_PACKED, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
//  printf("recebeu...%d\n",r);

  // obtain the structure fields from the buffer
  MPI_Unpack(buffer, bufsize, &position, &msg.step, 1, MPI_INT, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, &msg.source, 1, MPI_INT, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, &msg.sink, 1, MPI_INT, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, &msg.flow, 1, MPI_DOUBLE, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, &msg.side, 1, MPI_CHAR, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, &msg.size, 1, MPI_INT, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, &msg.time_per_cut, 1, MPI_DOUBLE, MPI_COMM_WORLD);
  MPI_Unpack(buffer, bufsize, &position, msg.nodes, msg.size, MPI_INT, MPI_COMM_WORLD);

  return status.MPI_SOURCE;
}

// filename is outpath + basename(input_file) + ".stats.txt" 
// nmax is the size allocated to stats_filename
void buildStatsFileName(char *stats_filename, char *outpath, char *input_file, int nmax)
{
  char *begin;
  int i, j, k;

  // get the basename
  begin = basename(input_file);
  // find the last dot
  for (i=strlen(begin)-1; begin[i] != '.' && i>1; i--)
    ;
  // start with the outpath
  j=0;
  if (strcmp(outpath, "")) {
    for ( ; j < strlen(outpath); j++)
      stats_filename[j] = outpath[j];
    stats_filename[j++] = '/';
  }
  // append the basename without the extension
  for (k=0; k < i && k < nmax-1; k++)
    stats_filename[j++] = begin[k];
  stats_filename[j] = '\0';
  strncat(stats_filename, ".stats.txt", nmax-strlen(stats_filename)-1);
}

int main (argc, argv)
     int   argc;
     char *argv[];

{
#if (defined(PRINT_FLOW))
  node *i;
  arc *a;
#endif

#ifdef PRINT_FLOW
  long ni, na;
#endif

  node *j;

  int  cc;

  if (argc < 2) {
    printf("Usage: %s inputfile [run# outpath]\n", argv[0]);
    exit(1);
  }

  globUpdtFreq = GLOB_UPDT_FREQ;

#ifdef OUTPUT_STATS
  char *outpath;
  int run_number;

  if (argc >= 3)
    run_number = atoi(argv[2]);
  else run_number = 1;

  if (argc == 4) 
    outpath = argv[3];
  else
    outpath = "";
#endif

  int numprocs, rank, rc, tag=1;  
  int st[2];

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN); //the program will not longer abort on having detected an MPI error, instead the error will be returned and you will have to handle it

  if (numprocs < 2) {
    printf("Please run with minimum 2 processes.\n");fflush(stdout);
    MPI_Finalize();
    return 1;
  }

  parse(argv[1], &n, &m, &nodes, &arcs, &cap, &nMin );
  
  MPI_Status Stat;

  bufsize = n*sizeof(int) + sizeof(msg);
  buffer = calloc(bufsize, sizeof(int)); //buffer to send and receive answers: struct fields + nodes
  msg.nodes = calloc(n, sizeof(long));

  double sendbuf[3], recvbuf[3]={0.0,0.0,0.0}; //to reduce real, user e sys times

  timer1();
 
  if (rank==0) { //master  
    long  unsuccess=0; 

    int *tree; 
    double *tree_weights;
    int step; 

#ifdef OUTPUT_STATS
    FILE *stats_file;
    char stats_filename[201];

    buildStatsFileName(stats_filename, outpath, argv[1], 200);
    stats_file = fopen(stats_filename, "a");
#endif

    // initialization for gusfield 
    tree = calloc(n+2, sizeof(long)); 
    tree_weights = calloc (n+2, sizeof(double));
    if (tree == NULL || tree_weights == NULL) {
      printf("can't obtain enough memory to solve this problem.\n");
      exit(1);
    }

    tree[0] = -1; 
    tree_weights[0] = -1.0;
    for (step=1; step < n; ++step) 
    {
      tree[step] = 0;
      tree_weights[step] = -1.0;
    }

    // gusfield iteration starts here 
    if (numprocs>n)
       numprocs = n;
    
    for (step=1; step<numprocs; step++) {
       st[0] = step;
       st[1] = tree[step];
       int to = step;
       rc = MPI_Send(&st, 2, MPI_INT, to, tag, MPI_COMM_WORLD);
    }

    long done = 0;
    while(done < n-1) {
      int from = receiveAnswer();
#ifdef OUTPUT_STATS
      // output statistics:
      //    num_procs  run_number  proc  s  t  flow  time_per_cut
      fprintf(stats_file, "%5d %5d %5d ", numprocs, run_number, from);
      fprintf(stats_file, "%6d %6d %14.2lf %14lf\n",  msg.source, msg.sink, msg.flow, msg.time_per_cut);
#endif
      if (msg.sink == tree[msg.step]) {
          done++;
          tree_weights[msg.step] = msg.flow;
          int x;  
          for (x=0; x<msg.size; x++) {
             int pos = msg.nodes[x];
             if (tree_weights[pos] < 0.0 && (tree[pos] == tree[msg.step]))
               tree[pos] = msg.step;
          }          
          st[0] = step;
          st[1] = tree[step];
          step++;
          if (step<=n) { //new subproblem
            int to = from;
            rc = MPI_Send(&st, 2, MPI_INT, to, tag, MPI_COMM_WORLD);
	    // printf("c Proccess %d sent s= %ld t=%ld to %d\n", rank, st[0], st[1], to);
          }
      } else { // the task has failed, send another one
          unsuccess++;
          st[0] = msg.step;
          st[1] = tree[msg.step];
          int to = from;
          rc = MPI_Send(&st, 2, MPI_INT, to, tag, MPI_COMM_WORLD);
      }
    } // gus iteration ends here

    // begin: print the flow equivalent tree
    printf("c flow equivalent tree\n");
    printf("p cut %ld %ld\n", n, n-1);
    for (step = 1; step < n; ++step)
        printf("a %d %d %.1lf\n", step+1, tree[step]+1, tree_weights[step]);    

    timer2();
    sendbuf[0] = getRealTime();
    sendbuf[1] = getUserTime();
    sendbuf[2] = getSystemTime();

    //sending source=-1 to all slaves to make then stop
    for (step=1; step<numprocs; step++) {
       st[0] = -1;
       st[1] = -1;
       int to = step;
       rc = MPI_Send(&st, 2, MPI_INT, to, tag, MPI_COMM_WORLD);
    }
#ifdef OUTPUT_STATS
    fclose(stats_file);
#endif
    //printf("c Thread %d finalized. timer: real: %.7lf user: %.7lf sys: %.7lf\n", rank, sendbuf[0], sendbuf[1], sendbuf[2]); 
    MPI_Reduce( &sendbuf, &recvbuf, 3, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    printf("c timer: real: %.7lf user: %.7lf sys: %.7lf\n", sendbuf[0], recvbuf[1], recvbuf[2]); 
    printf("c success: %ld unsuccess: %ld\n", done, unsuccess);

    MPI_Finalize();
    return (0);
    
  } else { //slaves
    timer1();
    cc = allocDS();
    while (1) {
      if ( cc ) { printf ("Allocation error\n"); exit ( 1 ); }
      int from = 0;
      rc = MPI_Recv(&st, 2, MPI_INT, from, tag, MPI_COMM_WORLD, &Stat);
      if (rc != MPI_SUCCESS || st[0] < 0) {
         timer2();
         sendbuf[0] = getRealTime();
         sendbuf[1] = getUserTime();
         sendbuf[2] = getSystemTime();
         
         MPI_Reduce( &sendbuf, &recvbuf, 3, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
         MPI_Finalize();
         return (0);
      }

      source = nodes + st[0]; 
      sink = nodes +  st[1];

      init();
      stageOne ( );
      stageTwo ( );

#ifdef PRINT_STAT
      printf ("c pushes:      %10ld\n", pushCnt);
      printf ("c relabels:    %10ld\n", relabelCnt);
      printf ("c updates:     %10ld\n", updateCnt);
      printf ("c gaps:        %10ld\n", gapCnt);
      printf ("c gap nodes:   %10ld\n", gNodeCnt);
      printf ("c\n");
#endif

#ifdef PRINT_FLOW
      printf ("c flow values\n");
      forAllNodes(i) {
	ni = nNode(i);
	forAllArcs(i,a) {
	  na = nArc(a);
	  if ( cap[na] > 0 )
	    printf ( "f %7ld %7ld %12ld\n",
		     ni, nNode( a -> head ), cap[na] - ( a -> resCap )
		     );
	}
      }
      printf("c\n");
#endif

      globalUpdate(); 
      /*
      printf ("c nodes on the sink side\n");
      forAllNodes(j)
	if (j->d < n)
	  printf("c %ld ", nNode(j));
      printf("\n");
      */

      //send results to master
      msg.step = st[0];
      msg.source = st[0];
      msg.sink = st[1];
      msg.flow = flow;
      msg.side = 's';
      msg.size = 0;
      forAllNodes(j) {
	if (j->d >= n) { // j is on the source side
	  msg.nodes[msg.size]= Index(j);
          msg.size++;
        }
      }
      msg.time_per_cut = timer() - t;
      rc = sendAnswer(t);
    } //end while (true) 
  }
  return 0;
  

}
