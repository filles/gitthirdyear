// Hippie problem:
// 4 Hippies want to cross a bridge. The bridge is fragile, it can only crossed
// by <= 2 people at a time with a torchlight. The hippies have one torchlight
// and want to reach the other side within one hour. Due to different degrees
// of intoxication they require different amounts of time to cross the bridge:
// 5, 10, 20 and 25 minutes. If a pair crosses the bridge, they can only move
// at the speed of the slower partner.
//
// A PROMELA model for this problem is shown below.
// To find a solution we use SPINs verifier mode to check if the assertion at 
// the bottom (which claims the problem is unsovable) holds:
//
// ./spin -a hippies.pr               *this generates the verifier in pan.c*
// gcc -O3 pan.c -o hippies           *compile the verifier*
// ./hippies                          *run the verifier, assertion is violated*
// ./hippies -r hippies.pr.trace      *inspect the trace to get the solution*  

int time;
bit lamp;
bit pos[4];
int times[4]
int bridge[2];

proctype H() {
	do
	:: (time < 60) ->

		// pick two random hippies from the side of the lamp and move them
		int cnt = 0;
		do
		:: (cnt < 2) ->
			if	
				:: (pos[0] == lamp) -> bridge[cnt] = 0; pos[bridge[cnt]] = !pos[bridge[cnt]]; cnt++; printf("hippie %d is going\n", times[0]);
				:: (pos[1] == lamp) -> bridge[cnt] = 1; pos[bridge[cnt]] = !pos[bridge[cnt]]; cnt++; printf("hippie %d is going\n", times[1]);
				:: (pos[2] == lamp) -> bridge[cnt] = 2; pos[bridge[cnt]] = !pos[bridge[cnt]]; cnt++; printf("hippie %d is going\n", times[2]);
				:: (pos[3] == lamp) -> bridge[cnt] = 3; pos[bridge[cnt]] = !pos[bridge[cnt]]; cnt++; printf("hippie %d is going\n", times[3]);
				:: else -> skip;
			fi
		:: (cnt >= 1) ->
			break;
		od;

		// move the lamp in the time the slower one needs
		lamp = !lamp;
		if 
		:: ((cnt == 2) && (times[bridge[1]] > times[bridge[0]])) -> 
			time = time + times[bridge[1]];
		:: else ->
			time = time + times[bridge[0]];
		fi

		// show the state after the move
		printf("hippies: [%d %d %d %d] -- lamp: %d -- time: %d\n",
		       pos[0], pos[1], pos[2], pos[3], lamp, time);

	:: (time >= 60) -> 
		break;
	od;

	assert((time > 60) || ((pos[0] + pos[1] + pos[2] + pos[3]) < 4));
}

init {
	times[0] = 5;
	times[1] = 10;
	times[2] = 20;
	times[3] = 25;
	run H();		
}
