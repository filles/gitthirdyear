
//Kommentare: 
//festgelegt: input graph darf node 0 nichtenthalten
//
//
#include <iostream>
#include <boost/thread.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/stoer_wagner_min_cut.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/one_bit_color_map.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/graph/directed_graph.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
//#include <boost/graph/read_dimacs.hpp>

// using namespace boost;

    double alpha= 1.;


    typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;
	template <class OutEdgeListS = boost::vecS,
	// a Sequence or an AssociativeContainer class VertexListS = vecS,
	// a Sequence or a RandomAccessContainer class DirectedS = directedS,
	class VertexProperty = boost::no_property,
	class EdgeProperty = EdgeWeightProperty,
	class GraphProperty = boost::no_property,
	class EdgeListS = boost::listS>
	class adjacency_list {  };
	typedef boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS, boost::no_property, EdgeWeightProperty> mygraph;
    typedef boost::property_map<mygraph, boost::edge_weight_t> weight_map_type;
    typedef boost::property_traits<weight_map_type> weight_type;


// fuer den flow graphen: 
 	typedef boost::adjacency_list_traits<boost::vecS, boost::vecS, boost::directedS> Traits;
 	typedef boost::property<boost::edge_capacity_t, long, boost::property<boost::edge_residual_capacity_t, long, boost::property<boost::edge_reverse_t, Traits::edge_descriptor>>> EdgeFlowProperty;
	typedef boost::adjacency_list<boost::listS, boost::vecS, boost::directedS, boost::no_property, EdgeFlowProperty> flow;

mygraph CalcMinCutTree(mygraph g);


int main(){
    std::cout << "Hello" << std::endl;
    mygraph g;
    
    //1. cluster
    boost::add_edge (1, 2, 5, g);
    boost::add_edge (1, 3, 5, g);
    
    //2. cluster
    boost::add_edge (4, 5, 7, g);
    boost::add_edge (4, 7, 7, g);
    boost::add_edge (5, 6, 7, g);
    boost::add_edge (6, 7, 7, g);
    
    //3. cluster
    boost::add_edge (8, 9, 9, g);
    boost::add_edge (8, 10, 9, g);
    boost::add_edge (9, 10, 9, g);
    
    //Verbindungen
    boost::add_edge (1, 4, 2, g);
    boost::add_edge (7, 8, 1, g);
    
    
    mygraph::vertex_iterator vertexIt, vertexEnd;
    mygraph::adjacency_iterator neighbourIt, neighbourEnd;
    boost::tie(vertexIt, vertexEnd) = vertices(g);
    for (; vertexIt != vertexEnd; ++vertexIt)
    {
        std::cout << *vertexIt << " is connected with ";
        boost::tie(neighbourIt, neighbourEnd) = boost::adjacent_vertices(*vertexIt, g);
        for (; neighbourIt != neighbourEnd; ++neighbourIt)
            std::cout << *neighbourIt << " ";
        std::cout << "\n";
    }
    
    CalcMinCutTree(g);


    return 0;
}



mygraph CutClusterBasic(mygraph g, double alpha){
	//using namespace boost 
	mygraph::vertex_iterator vertexIt, vertexEnd;
 	boost::tie(vertexIt, vertexEnd) = vertices(g);

	//add artificial Sink 0 connected to all nodes with edgevalue alpha
	//add_vertex(0,g); <- nicht sicher ob notwenig / richtig
	for (int i = 0; vertexIt != vertexEnd; ++vertexIt, ++i){ 
	boost::add_edge(0,i,alpha,g);
	}	

	mygraph tempgraph = g;	

	CalcMinCutTree(tempgraph);

	boost::clear_vertex(0,g);
	boost::remove_vertex(0,g);

	return g;

}


mygraph CalcMinCutTree(mygraph g){
	// das is noch vom min cut stoer wagner .. 
    //nodes with same parity are on the same side of the min-cut
    //BOOST_AUTO(parities, make_one_bit_color_map(num_vertices(g), get(vertex_index, g)));

	// make the extra tree which we will use for 'bookkeeping'

	// sterngraph mit |nodes(g)| nodes also for 1 to |..| 
	// wird ein Gomory-Hu tree
	mygraph gmhu ;
	// was passiert wenn ich das mache gmhu = g;
	int n = boost::num_vertices(g);
	for (int i = 1; i<=n ;  ++i){ 
		boost::add_edge(0,i,gmhu);
	}

	//rechnet n-1 min cuts aus speichert in den gmhu
	//macht einen directed graphen den wir fuer die s-t cuts brauchen 
	flow f; 
	mygraph::edge_iterator edgeIt, edgeEnd;
 	boost::tie(edgeIt, edgeEnd) = boost::edges(g);
	boost::graph_traits<mygraph>::edge_descriptor e;
	boost::graph_traits<flow>::vertex_descriptor s;
	boost::graph_traits<flow>::vertex_descriptor t;

	// sollte edges aus g als zwei gerichtete Kanten in f einfuegen  
	for ( ;edgeIt != edgeEnd; edgeIt++){ 
		e = *edgeIt;
		//int in = boost::target(a,g);
		//int ou = int(boost::source(a,g));
		//boost::add_edge(e,1.,f);
		boost::add_edge(boost::source(e,g),boost::target(e,g),1.,f);
		boost::add_edge(boost::target(e,g),boost::source(e,g),1.,f);
	}	

    flow::vertex_iterator vertexIt, vertexEnd;
    boost::tie(vertexIt, vertexEnd) = vertices(f);
	//set the 0 and start s from 2 
	t= *vertexIt;
	++vertexIt;

	for (; vertexIt != vertexEnd; ++vertexIt){ 
		s = *vertexIt;	
	//boost::push_relabel_max_flow(f, t,s);
		std::cout << "result" << std::endl;
	//for( int s = 2; s <= n; ++s){
		//choose first s : 

		
		

	// das hier geht nicht, da wir immer einen globalen min Cut berechnen
	// wir brauchen einen s-t min cut bzw. s-t max flow
	// endmonds-karp ect. brauchen einen gerichteten graph als input mit capacites ect.
	// to-do: 
	// 0. (double checken ob stoer wagner nicht s und t als input zulässt..) 
	// 1. erzeuge hier einen ReverseEdgeMap grapghen also halt fuer restcapacities
	// 2. calle kolmogorov- oder push_relabel_ max_flow algo 
	// 3. splitte bei den max flow edges?
	// 4. hänge alle nodes > s die im s schnitt sind an s und ab von 0
	//    und setze die edge von s und t auf den wert vom erhaltenen min- cut
	// 5. suche von s+1 den unique nachbarn (sollte immer eindeutig sein) 
	// 6. rinse and repeat till alle einmal durch 
	// 7. et voila fertiger Gomory- Hu tree 
	// for further reference consult paper with the Crabs in slack :) 
	//
	//
	//
    //	int w = stoer_wagner_min_cut(g, get(edge_weight, g), parity_map(parities));
    //	for (int i = 1; i < num_vertices(g); ++i) {
    //	    if (get(parities, i))
	}
	return gmhu;	
}
