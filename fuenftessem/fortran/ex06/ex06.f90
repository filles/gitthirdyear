     program stokes 
        use del2_mod_nocomplex 
        use Vcycle  
        implicit none
        integer:: nx=1, ny=1,nxc,nyc, i, j, width
        character(len = 6)::  Tinit ="random"
        real:: a=1., h, res_rms, resV, f_rms=0
        !n
        real:: err, Ra, total_time,a_dif,a_adv, dt_dif, &
                k =1., curr = 0.,dt_min

        logical :: multigrid= .true.
        real,allocatable:: u_f(:,:),res_c(:,:),corr_c(:,:),&
                          res_f(:,:), corr_f(:,:),&
                          rhs(:,:), f(:,:)
        !n
        real,allocatable:: T(:,:), S(:,:), W(:,:),&
            V(:,:,:), T_temp(:,:),temp_T_2(:,:)
        real,parameter :: pi =3.1415926


        namelist /in/ ny,total_time,Ra,err,a_dif,a_adv,&
            nx, Tinit 

        open(1,file="relaxIN.txt", status="old")
        read(1,in)
        close(1)

        allocate(u_f(nx,ny),res_c(nx,ny),corr_c(nx,ny),&
                  res_f(nx,ny), corr_f(nx,ny),rhs(nx,ny),&
                  temp_T_2(nx,ny))
        ! new part: 
        h = 1./(ny-1)
        !whats the value of k?
        dt_dif = a*h**2/k
        allocate(T(nx,ny), S(nx,ny), W(nx,ny),V(2,nx,ny),&
            T_temp(nx,ny)) 
            T_temp = 0. 
            rhs = Ra



        ! filling the T 
        u_f = 0.;  S = 0.;  W = 0.;  V = 0.;  T = 0.
        width = nx /ny
        if(Tinit == "cosine") then 
            do i= 1,nx
                do j= 1,ny
                    T(i,j) = 0.5*(1+ cos(3*pi*i/width))*&
                        (1+ cos(3*pi*j/ny))
                end do 
            end do
        else
            call random_number(T)
        end if
        T(:,1) = 1.
        T(:,ny) = 0

       



        res_rms = 1000



        !unfortunately i did not fully understand the exercise :(




        ! we set the v to rhs, which has been set to Ra 
        do while(total_time > curr)
        !calculating the rhs
           call upwind2d(T,nx,ny, rhs,h, T_temp)
           f = T_temp
        !solving to get W from rhs
           
           do i=1,nx-1
               do j=1,ny-1
                   f_rms = f_rms + f(i,j)**2
               end do
           end do 
           f_rms = sqrt(f_rms/(nx*ny))
           do while (res_rms/f_rms > err)
               res_rms = Vcycle_2DPoisson(u_f,f,h)
           end do 
           S = u_f
           call centeredfd(S,nx,ny,V,h)
           call upwind2d(S,nx,ny,V,h,T_temp)
           dt_min = min(dt_dif,a_adv*h/MAXVAL(T_temp))
           
            call upwind2d(T,nx,ny,V,h,T_temp)
            call eeul2d(T,nx,ny,h,temp_T_2)
            T = T + dt_min*(temp_T_2 - T_temp )
            T(1,:)= T (2,:)
            T(nx,:) = T(nx-1,:)
            curr = curr + dt_min


            dt_min = min(dt_dif,a_adv*h/MAXVAL(T_temp))

                    !making sure we dont get into an endless loop 
                    if(dt_min <= 0) exit;
            end do     
    

                open(3,file='finalT_2d_stokes.dat',status='replace')
                do j= 1,ny 
                   write(3,'(1000(1pe13.5))') T(:,j)
                end do
                close(3)
                

               deallocate(u_f,res_c,corr_c,f,&
                          res_f, corr_f,rhs,&
                          temp_T_2,T, S, &
                          W,V, T_temp)


            end program stokes  
