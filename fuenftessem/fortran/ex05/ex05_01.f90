            program poisson
                use Vcycle  
                implicit none
                integer:: nx=1, ny=1,nxc,nyc, i, j
                character(len = 5)::  init ="nospk"
                real:: alpha=0., h, res_rms, resV, f_rms=0
                real,parameter:: err = 1/10000.
                logical :: multigrid
                real,allocatable:: u_f(:,:),res_c(:,:),corr_c(:,:),&
                                   res_f(:,:), corr_f(:,:), f(:,:)


                namelist /in/ nx,ny,init,alpha,multigrid
                ! had some issue, so that i could not read the file
                ! properly .. :( 
                open(1,file="relaxIN.txt", status="old")
                read(1,in)
                close(1)
                !nx=129
                !ny=257
                !init='spike'
                !alpha=0.7
                !multigrid=.true.

                allocate(u_f(nx,ny),res_c(nx,ny),corr_c(nx,ny),&
                          res_f(nx,ny), corr_f(nx,ny),f(nx,ny))


                h = 1./(ny-1)

                ! filling the u_f
                u_f = 0.
                f   = 0.
                if(init == "spike") then 
                    f(nx/2+1,ny/2+1) =1./(h**2)
                else
                    call random_number(f)
                end if
                do i=1,nx-1
                    do j=1,ny-1
                        f_rms = f_rms + f(i,j)**2
                        !print*,i,j, f_rms
                    end do
                end do 
                f_rms = sqrt(f_rms/(nx*ny))


                res_rms = 1000
                    !print*, nx/2+1, ny/2+1
                if(multigrid) then 
                    do while (res_rms/f_rms > err)
                        res_rms = Vcycle_2DPoisson(u_f,f,h)
                        print*, res_rms/f_rms 
                    end do 
                else 
                    do while (res_rms/f_rms > err)
                        res_rms = iteration_2DPoisson(u_f,f,h,alpha)
                        print*, res_rms,f_rms 
                    end do 
                end if 
                    
    

                open(3,file='finalT_2d_poisson129.dat',status='replace')
                do j= 1,ny 
                   write(3,'(1000(1pe13.5))') u_f(:,j)
                end do
                close(3)
                
                deallocate(u_f,res_c,corr_c, res_f, corr_f,f)

            end program poisson 
