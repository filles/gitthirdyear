     module Vcycle
        contains
        
        recursive function Vcycle_2DPoisson(u_f,rhs,h) result (resV)
            implicit none
            real resV
            real,intent(inout):: u_f(:,:)  ! arguments
            real,intent(in)   :: rhs(:,:),h
            integer         :: nx,ny,nxc,nyc, i  ! local variables
            real,allocatable::res_c(:,:),corr_c(:,:),res_f(:,:), & 
                                corr_f(:,:)
            real            :: alpha=0.7, res_rms
        
            nx=size(u_f,1); ny=size(u_f,2)  ! must be power of 2 plus 1
            if( nx-1/=2*((nx-1)/2) .or. ny-1/=2*((ny-1)/2) ) &
                 stop 'ERROR:not a power of 2'
            nxc=1+(nx-1)/2; nyc=1+(ny-1)/2  ! coarse grid size


        
            if (min(nx,ny)>5) then  ! not the coarsest level
        

               allocate(res_f(nx,ny),corr_f(nx,ny), &
                    corr_c(nxc,nyc),res_c(nxc,nyc))
        
               !---------- take 2 iterations on the fine grid--------------
               res_rms = iteration_2DPoisson(u_f,rhs,h,alpha) 
               res_rms = iteration_2DPoisson(u_f,rhs,h,alpha)
        
               !---------- restrict the residue to the coarse grid --------
               call residue_2DPoisson(u_f,rhs,h,res_f) 
               call restrict(res_f,res_c)
        
               !---------- solve for the coarse grid correction -----------
               corr_c = 0.  
               res_rms = Vcycle_2DPoisson(corr_c,res_c,h*2) ! *RECURSIVE CALL*
        
               !---- prolongate (interpolate) the correction to the fine grid 
               call prolongate(corr_c,corr_f)
        
               !---------- correct the fine-grid solution -----------------
               u_f = u_f - corr_f  
        
               !---------- two more smoothing iterations on the fine grid---
               res_rms = iteration_2DPoisson(u_f,rhs,h,alpha)
               res_rms = iteration_2DPoisson(u_f,rhs,h,alpha)
        
               deallocate(res_f,corr_f,res_c,corr_c)
        
            else  
        
               !----- coarsest level (ny=5): iterate to get 'exact' solution
        
               do i = 1,100
                  res_rms = iteration_2DPoisson(u_f,rhs,h,alpha)
               end do
        
            end if
        
            resV = res_rms   ! returns the rms. residue
        
        end function Vcycle_2DPoisson

        function iteration_2DPoisson(u_f,rhs,h,alpha) result(res_rms)
            real,intent(inout):: u_f(:,:)  ! arguments
            real,intent(in)   :: rhs(:,:),h
            real,allocatable   :: res(:,:)
            integer         :: nx,ny, i,j  ! local variables
            real            :: alpha
            real   res_rms

            nx=size(u_f,1); ny=size(u_f,2)  
        
            allocate(res(nx,ny)); res=0
            res_rms =0

            do i=2,nx-1
                do j=2,ny-1
                    res(i,j) = (u_f(i,j+1)+u_f(i,j-1)+ u_f(i+1,j)+&
                        u_f(i-1,j)-4*u_f(i,j))/(h**2) -rhs(i,j)
                    u_f(i,j) =  u_f(i,j) + alpha * res(i,j)*(h**2)/4.
                end do 
            end do
            do i=1,nx
                do j=1,ny
                    res_rms = res_rms + res(i,j)**2 
                end do 
            end do
            res_rms = sqrt(res_rms/(ny*nx))


        end function iteration_2DPoisson
            
        subroutine residue_2DPoisson(u_f,rhs,h,res_f) 
            real,intent(inout):: u_f(:,:), res_f(:,:)
            real,intent(in)   :: rhs(:,:),h
            integer         :: nx,ny, i,j  ! local variables

            nx=size(u_f,1); ny=size(u_f,2)  
                
            do i=2,nx-1
                do j=2,ny-1
                    res_f(i,j) = (u_f(i,j+1)+u_f(i,j-1)+u_f(i+1,j)+&
                                u_f(i-1,j)-4*u_f(i,j)) /(h**2) -rhs(i,j)
                end do 
            end do

        end subroutine residue_2DPoisson

        subroutine restrict(res_f,res_c) 
            implicit none
            real,intent(inout):: res_c(:,:), res_f(:,:) ! arguments
            integer         :: nx,ny, i,j  ! local variables
            nx=size(res_c,1); ny=size(res_c,2)  

            do i=1,nx
                do j=1,ny
                    res_c(i,j) = res_f(2*i-1,2*j-1)
                end do 
            end do
            
        end subroutine restrict

       subroutine prolongate(res_c,res_f)
           implicit none
           real,intent(inout):: res_c(:,:), res_f(:,:) ! arguments
           integer         :: nx,ny, i,j  ! local variables
           nx=size(res_f,1); ny=size(res_f,2)  

           do i =1, nx,2
             do j = 1,ny,2
                res_f(i,j)= res_c((i+1)/2,(j+1)/2)
             end do 
           end do
           do i =2, nx-1,2
             do j = 1,ny,2
                res_f(i,j)=(res_c(i/2,(j+1)/2)+&
                    res_c((i/2)+1,(j+1)/2))/2.
             end do 
           end do
           do i =1, nx
             do j = 2,ny-1,2
                res_f(i,j)=(res_f(i, j-1)+ res_f(i,j+1))/2.
             end do 
           end do

       end subroutine prolongate
        
     end module Vcycle
