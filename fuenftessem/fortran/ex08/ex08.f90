     program stokes 
        use del2_mod_nocomplex 
        use Vcycle  
        implicit none
        integer:: nx=257, ny=65,nxc,nyc, i, j, width
        character(len = 6)::  Tinit ="cosine"
        real:: a=1., h, res_rms, resV, f_rms=0
        !n
        real:: err=1.e-3, Pr, total_time=0.1,a_dif=0.15,a_adv=0.4,&
                dt_dif, k =1., curr = 0.,dt_min, Ra =1.e6

        logical :: multigrid= .true.
        real,allocatable:: u_f(:,:),res_c(:,:),corr_c(:,:),&
                          res_f(:,:), corr_f(:,:),&
                          rhs(:,:), f(:,:)
        real,allocatable:: T(:,:), S(:,:), W(:,:),&
            V(:,:,:), T_temp(:,:),temp_T_2(:,:),temp_T_3(:,:)
        real,parameter :: pi =3.1415926


        !namelist /in/ Pr,ny,total_time,Ra,err,a_dif,a_adv,&
        !    nx, Tinit 
        namelist/inputs/ Pr,nx, ny, total_time, Ra, err, a_dif, a_adv,&
            Tinit

        open(1,file="lowPrandt_parameters.txt", status="old")
        read(1,inputs)
        close(1)

        allocate(u_f(nx,ny),res_c(nx,ny),corr_c(nx,ny),&
                  res_f(nx,ny), corr_f(nx,ny),rhs(nx,ny),&
                  temp_T_2(nx,ny), temp_T_3(nx,ny))
        ! new part: 
        h = 1./(ny-1)
        !whats the value of k?
        dt_dif = a*h**2/max(1.,Pr)
        allocate(T(nx,ny), S(nx,ny), W(nx,ny),V(2,nx,ny),&
            T_temp(nx,ny)) 
            T_temp = 0. 
            rhs = Ra



        ! filling the T 
        u_f = 0.;  S = 0.;  W = 0.;  V = 0.;  T = 0.
        width = nx /ny
        if(Tinit == "cosine") then 
            do i= 1,nx
                    T(i,:) = 0.5*(1+ cos(3*pi*i/width))
                end do 
        else
            call random_number(T)
        end if
        T(:,1) = 1.
        T(:,ny) = 0

        res_rms = 1000


        ! we set the v to rhs, which has been set to Ra 
        do while(total_time > curr)
        !calculating the rhs
           !call upwind2d(T,nx,ny, rhs,h, T_temp)
           !call centeredfd(rhs,T,h)
           f = T; temp_T_3 = T  ! storing the T7dx for the calculation of w
        !solving to get W from rhs
    
           f_rms =rms(f)
           do while (res_rms/f_rms > err)
               res_rms = Vcycle_2DPoisson(u_f,f,h)
           end do 
           !S = u_f
           W = u_f

           ! calculating the S by Poisson solving from W ?
           f_rms = rms(W)
           do while (res_rms/f_rms > err)
               res_rms = Vcycle_2DPoisson(u_f,f,h)
           end do 
           S = u_f

           !calculating vx and vy from S
           call centeredfd(S,V,h)
           call upwind2d(S,V,h,T_temp)
           
            call upwind2d(T,V,h,T_temp)
            call eeul2d(T,nx,ny,h,temp_T_2)
            T = T + dt_min*(temp_T_2 - T_temp )
            T(1,:)= T (2,:)
            T(nx,:) = T(nx-1,:)

            call upwind2d(W,V,h,T_temp)
            call eeul2d(W,nx,ny,h,temp_T_2)

            W = W + dt_min*(Pr*temp_T_2 - T_temp - Ra*Pr*temp_T_3 )
            curr = curr + dt_min


            dt_min = min(dt_dif,a_adv*h/MAXVAL(T_temp))

                    !making sure we dont get into an endless loop 
                    if(dt_min <= 0) exit;
            end do     
    

                open(3,file='finalT_2d_stokes.dat',status='replace')
                do j= 1,ny 
                   write(3,'(1000(1pe13.5))') T(:,j)
                end do
                close(3)
                

               deallocate(u_f,res_c,corr_c,f,&
                          res_f, corr_f,rhs,&
                          temp_T_2,T, S, &
                          W,V, T_temp,temp_T_3)


            end program stokes  
