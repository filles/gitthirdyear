            module del2_mod_nocomplex
            contains

                !this just calculates the normal deriv by using 1d euler
                subroutine eeul1d (T, h, dT )
                    real, intent(in)    :: T(:), h
                    real, intent(out)   :: dT(:)
                    integer             :: i, nx
                    nx= size(T)

                    do i=1,nx-1
                        dT(i) = (T(i+1)-T(i))/h
                    end do 
                    dT(nx) = 0.

                end subroutine eeul1d

                !subroutine to call eeul1d for 2d Arrays . bit of a
                !Workaround
                subroutine dTdx (T, h, dT )
                    real, intent(in)    :: T(:,:), h
                    real, intent(out)   :: dT(:,:)
                    integer             :: i, nx
                    nx= size(T,DIM=1)

                    dT = 0.
                    do i=1,nx-1
                        dT(i,:) = (T(i+1,:)-T(i-1,:))/(2*h)
                    end do 

                end subroutine dTdx

                ! this subroutine calculates k * Laplace of T by
                ! explicit Euler 2d for even Grid spacing
                subroutine eeul2d (T, h, d2T)
                    real, intent(in) :: T(:,:), h 
                    real, intent(out) :: d2T(:,:)
                    integer             :: i, j , nx, ny
                    nx= size(T,DIM=1); ny= size(T,DIM=2)
                    
                    do i= 2,nx-1 
                        do j= 2,ny-1
                            d2T(i,j) = (T(i-1,j)+T(i+1,j)+T(i,j-1) &
                                 +T(i,j+1)-4*T(i,j))/(h**2)*1.
                        end do 
                    end do
                end subroutine eeul2d


                subroutine centeredfd (S, dS, h )
                    real, intent(in)    :: S(:,:), h 
                    ! in order to not make 2 Matricies we use the real
                    ! part for vx and the imaginary for vy 
                    real,intent(out)    :: dS(:,:,:)
                    integer             :: i, j, nx , ny
                    nx= size(S,DIM=1); ny= size(S,DIM=2)

                    !use centered difference approx
                    do i= 2,nx-1 
                        do j= 2,ny-1
                         dS(1,i,j) =  (S(i,j+1)-S(i,j-1))/(2*h)
                         dS(2,i,j) =  -(S(i+1,j)-S(i-1,j))/(2*h)
                        end do 
                    end do
                    
                end subroutine centeredfd
                
                subroutine upwind2d (S, dS, h, tempS )
                    integer             :: i, j, nx ,ny
                    real, intent(in)    :: S(:,:), h 
                    real, intent(out)   :: tempS(:,:) 
                    ! in order to not make 2 Matricies we use the real
                    ! part for vx and the imaginary for vy 
                    ! may be possible to implement with pair or so
                    real,intent(in)  :: dS(:,:,:)

                    nx= size(S,DIM=1); ny= size(S,DIM=2)

                    !use forward   difference approx
                    do i= 2,nx-1 
                        do j= 2,ny-1
                            if(dS(1,i,j) > 0) then 
                             tempS(i,j) = dS(1,i,j)*&
                                            (S(i,j)- S(i-1,j))/h
                            else
                             tempS(i,j) = dS(1,i,j)*&
                                            (S(i+1,j)- S(i,j))/h
                            end if 

                            if(dS(2,i,j) > 0) then 
                             tempS(i,j) = tempS(i,j) + dS(2,i,j)*&
                                            (S(i,j)- S(i,j-1))/h

                            else
                             tempS(i,j) = tempS(i,j) + dS(2,i,j)*&
                                            (S(i,j+1)- S(i,j))/h
                            end if 
                        end do 
                    end do
                    
                end subroutine upwind2d

                function rms(f ) result (f_rms)
                    implicit none
                    real,intent(in) :: f(:,:)
                    real:: f_rms
                    integer :: nx,ny,i,j
                    nx= size(f,DIM=1)
                    ny= size(f,DIM=2)

                    f_rms = 0.
                    do i=1,nx-1
                        do j=1,ny-1
                            f_rms = f_rms + f(i,j)**2
                        end do
                    end do 
                    f_rms = sqrt(f_rms/(nx*ny))
                end function



            end module del2_mod_nocomplex





