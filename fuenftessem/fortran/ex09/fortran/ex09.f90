     program stokes
        use del2_mod_nocomplex
        use Vcycle_c
        implicit none
        integer::   nx =257, ny=65, i,j
        character(len = 6)::  Tinit ="cosine"

        real:: err=1.e-3,Pr=0.1, Ra = 1.e6, total_time=0.1, curr = 0., &
            a_dif= 0.15, a_adv=0.4, dt_dif, dt_adv, dt_min, k =1., h, &
            res_rms= 10 , resV, f_rms=0., width, beta=0, c=1;

        real,parameter :: pi=3.1415926

        real,allocatable:: T(:,:), S(:,:), W(:,:), V(:,:,:),&
            d2T(:,:), vgradT(:,:), dTbx(:,:),&
            d2W(:,:), vgradW(:,:), &
            f(:,:), u_f(:,:), res_f(:,:),&
            corr_c(:,:), corr_f(:,:), &
            rhs(:,:)

        namelist/inputs/ Pr, nx, ny, total_time, Ra, err, a_dif, a_adv,&
            Tinit, beta

        open(1,file="lowPrandt_parameters.txt", status="old")
        read(1,inputs)
        close(1)

        h = 1./(ny-1)
        dt_dif = a_dif*h**2/max(1.,Pr)
        allocate(T(nx,ny), S(nx,ny), W(nx,ny), V(2,nx,ny),&
            d2T(nx,ny), vgradT(nx,ny), dTbx(nx,ny),&
            d2W(nx,ny), vgradW(nx,ny), &
            f(nx,ny), u_f(nx,ny), res_f(nx,ny),&
            corr_c(nx,ny), corr_f(nx,ny), &
            rhs(nx,ny) )
        width = (nx-1.) /(ny-1.)


        !initialize T & w fields 
        if(Tinit == "cosine") then 
            do i= 1,nx
                    T(i,:) = 0.5*(1+ cos(3*pi*(i-1)*h/width))
            end do 
        else
            call random_number(T)
        end if
        T(:,1) = 1.; T(:,ny) = 0
        S = 0.; V = 0.
   

        ! calculate rhs from T
        !not sure here about the directions
        call dTdx(T,h,rhs)

        ! the error is here with the Ra the rms will become so small
        ! that is roundet to 0  then we get a floating point exception
        ! because we are dividing by zero :/ I dont know how to fix this 
        rhs = rhs !* Ra
        
        !calculate W from rhs
        f_rms = rms(rhs)
       
        do while (res_rms/f_rms > err)
            res_rms = Vcycle_2DPoisson_c(W,rhs,h,c )
        end do 

        !Timestepping
        do while(total_time > curr)

            !calculate S from W
            f_rms = rms(W)
            ! set res_rms to get into the loop
            res_rms= 1.
            do while (res_rms/f_rms > err)
                res_rms = Vcycle_2DPoisson_c(S,W,h,c)
            end do 

            !calculate V from S 
            call centeredfd(S,V,h)

            !take min timestep 
            !ignoring diffusive timestep if beta >= 0.5
            if (beta >=0.5) then 
                dt_min = a_adv*h/MAXVAL(abs(V(1,:,:)))
            else
                dt_min = min( dt_dif,a_adv*h/MAXVAL(abs(V(1,:,:))))
            end if 

            !calculate del2T
            call eeul2d(T,h,d2T)

            !calculate del2W
            call eeul2d(W,h,d2W)

            !calculate dTdx
            call dTdx(T,h,dTbx)

            !calculate vgradT
            call upwind2d(T,V,h,vgradT)

            !calculate vgradW
            call upwind2d(W,V,h,vgradW)

            !calculate new T
            if(beta .eq. 0) then
                T = T + dt_min*(d2T- vgradT)
            else  
                c = 1/(beta*dt_min)
                f =-c*(T+(dt_min*((1-beta)*d2T-vgradT)))

                !call the solver
                f_rms = rms(f)
                ! set res_rms to get into the loop
                res_rms= 1.
                do while (res_rms/f_rms > err)
                    res_rms = Vcycle_2DPoisson_c(T,f,h,c)
                end do 
            end if

            T(1,:)= T (2,:)
            T(nx,:) = T(nx-1,:)

            !calculate new W
            if(beta .eq. 0) then 
                W = W + dt_min*(Pr*d2W - vgradW - Ra*Pr*dTbx)
            else
                c = 1/(Pr*beta*dt_min)
                f =-c*(W+(dt_min*(Pr*(1-beta)*d2W-(vgradW)-Ra*Pr*dTbx)))
    
                !call the solver
                f_rms = rms(f)
                ! set res_rms to get into the loop
                res_rms= 1.
                do while (res_rms/f_rms > err)
                    res_rms = Vcycle_2DPoisson_c(W,f,h,c)
                end do 
            end if

            !update time
            curr = curr + dt_min

        end do 


        open(3,file='finalT_2d_NavierStokesT.dat',status='replace')
        do j= 1,ny 
           write(3,'(1000(1pe13.5))') T(:,j)
        end do
        close(3)
        open(2,file='finalT_2d_NavierStokesW.dat',status='replace')
        do j= 1,ny 
           write(2,'(1000(1pe13.5))') W(:,j)
        end do
        close(2)

        open(1,file='finalT_2d_NavierStokesS.dat',status='replace')
        do j= 1,ny 
           write(1,'(1000(1pe13.5))') S(:,j)
        end do
        close(1)

        open(4,file='finalT_2d_NavierStokesS.dat',status='replace')
        do j= 1,ny 
           write(4,'(1000(1pe13.5))') S(:,j)
        end do
        close(4)

        deallocate( T, S, W, V, &
            d2T, vgradT, dTbx,&
            d2W, vgradW, &
            f, u_f, res_f,&
            corr_c, corr_f, &
            rhs)


        end program stokes 
