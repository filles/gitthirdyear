            module del2_mod
            contains

                !this just calculates the normal deriv by using 1d euler
                subroutine eeul1d (T, nx, h, dT )
                    integer,intent(in)  :: nx 
                    real, intent(in)    :: T(nx), h
                    real, intent(out)   :: dT(nx)
                    integer             :: i 

                    do i=1,nx-1
                        dT(i) = (T(i+1)-T(i))/h
                    end do 
                    dT(nx) = 0.

                end subroutine eeul1d

                ! this subroutine calculates k * Laplace of T by
                ! explicit Euler 2d for even Grid spacing
                subroutine eeul2d (T, nx, ny, h, d2T)
                    integer,intent(in)  :: nx, ny 
                    real, intent(in) :: T(nx,ny), h 
                    real, intent(out) :: d2T(nx,ny)
                    integer             :: i, j 

                    do i= 2,nx-1 
                        do j= 2,ny-1
                            d2T(i,j) = (T(i-1,j)+T(i+1,j)+T(i,j-1) &
                                 +T(i,j+1)-4*T(i,j))/(h**2)*1.
                        end do 
                    end do
                end subroutine eeul2d


                subroutine centeredfd (S, nx, ny, dS, h )
                    integer, intent(in) :: nx,ny  
                    real, intent(in)    :: S(nx,ny), h 
                    ! in order to not make 2 Matricies we use the real
                    ! part for vx and the imaginary for vy 
                    double complex,intent(out)    :: dS(nx,ny)
                    integer             :: i, j

                    !use centered difference approx
                    do i= 2,nx-1 
                        do j= 2,ny-1
                        dS(i,j) = cmplx((S(i+1,j)-S(i-1,j))/(2*h), &
                                          -(S(i,j+1)-S(i,j-1))/(2*h))
                        end do 
                    end do
                    
                end subroutine centeredfd
                
                subroutine upwind2d (S, nx, ny, dS, h, tempS )
                    integer, intent(in) :: nx,ny  
                    real, intent(in)    :: S(nx,ny), h 
                    real, intent(out)   :: tempS(nx,ny) 
                    ! in order to not make 2 Matricies we use the real
                    ! part for vx and the imaginary for vy 
                    ! may be possible to implement with pair or so
                    double complex,intent(in)  :: dS(nx,ny)
                    integer             :: i, j

                    !use forward   difference approx
                    do i= 2,nx-1 
                        do j= 2,ny-1
                            if(DREAL(dS(i,j)) > 0) then 
                             tempS(i,j) = DREAL(dS(i,j))*&
                                            (S(i,j)- S(i-1,j))/h
                            else
                             tempS(i,j) = DREAL(dS(i,j))*&
                                            (S(i+1,j)- S(i,j))/h
                            end if 

                            if(DIMAG(dS(i,j)) > 0) then 
                             tempS(i,j) = DIMAG(dS(i,j))*&
                                            (S(i,j)- S(i,j-1))/h

                            else
                             tempS(i,j) = DIMAG(dS(i,j))*&
                                            (S(i,j+1)- S(i,j))/h
                            end if 
                        end do 
                    end do
                    
                end subroutine upwind2d
            end module del2_mod




