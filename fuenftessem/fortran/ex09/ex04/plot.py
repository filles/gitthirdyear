import numpy as np
import numpy.random
import matplotlib.pyplot as plt
import pandas as pd
#import seaborn as sns
import matplotlib.pylab as pltt
myTdata= pd.read_csv("finalT_2d_B1.dat",  sep="\s+")
#mySdata= pd.read_csv("finalT_2d_NavierStokesS.dat", sep="\s+")
#myWdata= pd.read_csv("finalT_2d_NavierStokesW.dat", sep="\s+")
# Create data
#x = np.random.randn(4096)
#y = np.random.randn(4096)

x = np.linspace(0.,4, 256)
y = np.linspace(0.,1, 64 )

# Create heatmap
#heatmap, xedges, yedges = np.histogram2d(x, y, bins=(256,64))
#heatmap, xedges, yedges = np.histogram2d(x, y, (x.shape[0], x.shape[1]))
mesh = np.meshgrid(x,y)
#extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
#ax = sns.heatmap(mesh, mydata)
pltt.show();


# Plot heatmap
plt.clf()
plt.title('solutions of ex08')
plt.ylabel('y')
plt.xlabel('x')
#plt.imshow(myTdata )
plt.imshow(myTdata, cmap='hot', interpolation='nearest',vmin=0, vmax =1)
#plt.imshow(myTdata, cmap='hot', interpolation='nearest')
#plt.imshow(mySdata, interpolation='nearest')
#plt.imshow(myWdata,  interpolation='nearest')
#fig, ax = plt.subplots()
#CS = ax.contour(myTdata)
#CS.show()
#plt.setp(CS.collections)
plt.show()
plt.savefig("finalT_2d_B1.png")
