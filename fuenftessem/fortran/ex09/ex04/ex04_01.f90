            program advection_diffusion2d
                use del2_mod_nocomplex
                implicit none
                integer:: nx=1, ny=1, i, j
                real:: total_time=0., k=1.0, a_dif=0.1, a_adv=0.2,& 
                    dt_d, h, curr=0., B =1., dt_min
                real,allocatable:: T(:,:), d2T(:,:), S(:,:),&
                    vgradT(:,:), dS(:,:,:)
                real,parameter :: pi =3.1415926


                namelist /inputs/ nx,ny,B,k,a_adv, a_dif, total_time

                open(1,file="AdvDif_parameters.dat", status="old")
                read(1,inputs)
                close(1)

                allocate(T(nx,ny),d2T(nx,ny),S(nx,ny),dS(2,nx,ny),&
                vgradT(nx,ny))

                h = 1./(ny-1)
                dt_d = a_dif*(h**2)/k

                ! filling the S
                do i= 1,nx
                    do j= 1,ny
                        S(i,j) = B* sin(pi*(i-1)/(nx-1))*&
                            sin(pi*(j-1)/(ny-1))
                    end do 
                end do

               call random_number(T)

               !     do i= 1,nx
               !         do j= 1,ny
               !             T(i,j) = 0
               !             if((i == nx/2) .and. (j == ny/2)) T(i,j)= 1.
               !         end do 
               !     end do 
               T(:,1) = 1.
               T(:,ny) = 0

            ! calculate (vx and vy) dS from S
                call centeredfd(S,dS,h) 
            ! calculate dt_adv from vx, vy
                call upwind2d(S,dS,h, vgradT)
            ! not sure if i am taking the right timesteps..
            dt_min = min(dt_d,a_adv*h/MAXVAL(dS(1,:,:)))
            !dt_min = min(dt_d,0.1*h)
            !dt_min = min(dt_dif,dt_adv)

               
            ! time = 0  solved since curr initialized as 0 

            ! Timestep
               i = 0
               do while (total_time > curr)

                    call upwind2d(T,dS,h, vgradT)
                    call eeul2d(T,h,d2T)
                    T = T + dt_min*(d2T - vgradT )
                    T(1,:)= T (2,:)
                    T(nx,:) = T(nx-1,:)
                    curr = curr + dt_min



                    !making sure we dont get into an endless loop 
                    if(dt_min <= 0) exit;

                    !if this condition is satisfied it is safe to assume
                    !we have reached the steadystate
                    if(vgradT(5,5) - d2T(5,5)<= 0.00000001 ) i = i+1
                    if( i == 100000) exit    

                end do 


                open(3,file='finalT_2d_B1.dat',status='replace')
                do j= 1,ny 
                   write(3,'(1000(1pe13.5))') T(:,j)
                end do
                close(3)
                
                deallocate(T,d2T, S, dS, vgradT)

            end program advection_diffusion2d 
