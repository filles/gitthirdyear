            program second_deriv
                implicit none
                integer          :: i, n 
                real, allocatable:: y(:), dydx_2(:), x(:), y_2(:)
                real             ::  dx 

                write(*,'(a,$)') "input number of Points on the Grid:"
                read*,n
                allocate(y(n),dydx_2(n), x(n), y_2(n))
                dx = 10./(n-1)
                
                do i= 1,n
                    x(i) = (i-1)*dx         !could add up the dx ?
                    y(i) = cos(x(i))   
                    y_2(i) = x(i)**2
                end do 

                !first function then second so we can reuse the dydx_2
                call second_derivative(y,n,dx,dydx_2)

                do i = 2,n-1
                    write(*,*) "Our deriv:", dydx_2(i) ,& 
                        'originalderiv:', cos(x(i)) , 'difference:',&
                        (cos(x(i))-dydx_2(i))
                end do 
                
                write(*,*) " now the second function: "

                call second_derivative(y_2,n,dx,dydx_2)

                do i = 2,n-1
                    write(*,*) "Our deriv:", dydx_2(i) ,& 
                        'originalderiv:', 2 , 'difference:',&
                        (2  - dydx_2(i))
                end do 
               
                deallocate(y,dydx_2, x, y_2)

                contains
                    subroutine second_derivative (y,n,dx,dydx_2)
                        integer,intent(in)  :: n
                        real,intent(in)     :: y(n), dx
                        real,intent(out)    :: dydx_2(n)
                        integer             :: i 

                        do i= 1,n-2
                            dydx_2(i+1) = (y(i+2)-2*y(i+1)+y(i))/(dx**2)
                        end do 

                        dydx_2(1)=0.
                        dydx_2(n) =0.
                    end subroutine second_derivative 
            end program second_deriv 
