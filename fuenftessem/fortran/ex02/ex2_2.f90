       program meanandderiv
        implicit none
        integer :: total, j
        real :: mean=0, deriv
        real,allocatable:: a(:)
        real,external::stdderivation 


        write(*,'(a,$)') "Please enter the number of inputs: "
        read*, total

        if (total < 0) then
             print*, "Error"
             
        else
             allocate(a(total))

             do j = 1, total
                  read*, a(j)
             end do            
             deriv = stdderivation(a,total,mean)

             print*, "mean: ", mean, "stddeviation: ", deriv
             deallocate(a)
        end if                 
       end program

         real  function stdderivation(a,n,mean)
          implicit none
          real,intent(in):: a(n)
          real,intent(out):: mean
          integer,intent(in):: n
          integer :: i
          real :: sums=0, sumsquared=0


         do i=1,n
             sums = sums + a(i)
             sumsquared =sumsquared +( a(i)**2)
         end do 
         mean = sums/n

         stdderivation = sqrt((sumsquared/n)-(mean**2))
        end function stdderivation 
