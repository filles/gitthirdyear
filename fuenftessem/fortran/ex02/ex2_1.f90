      program statements

        !the variables are named in order of the exercise a ->l


        implicit none 

        character(len=15):: a
        integer, parameter:: b = 5 
        integer:: c(-1:10)
        integer,allocatable:: d(:,:,:,:)

        !apparently one has to define the data before the first
        !function (conversion) so this is for the mod part:
        integer :: g=5, h=17, rem, sum =0, k
        integer:: l(1:100)

        


        integer e
        real:: f =2.75
        e = Nint(f)


        !
        print*,e, " and the original value is " , f


        !some testing and playing around
        allocate (d(2,3,4,2))
        d(2,3,3,1) = 2331
        d(1,3,3,1) = 1331
        print*,d
        deallocate(d)

        !what is meant by a and b exactly? its not totally clear from
        !the slides
        
        !integer :: g=5, h=17, rem 

        rem = mod(g,h)

        print*,rem

        !uses sum and k from above 
        do k = 12,124,2
            sum = k+ sum 
        enddo 

        do k = 1,100,1
            if (l(k) > 0 ) then 
                print*, "this integer is positive" 
                exit
            end if

        enddo 
        print*, sum 

       end program statements
