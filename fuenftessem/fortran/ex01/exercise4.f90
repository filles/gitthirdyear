       program mean
        
       implicit none

       integer :: total, j

       real :: temp, meann=0  , squared=0, deriv

       print*, "Please enter the number of inputs"

       read*, total
       
       if (total < 0) then

            print*, "Error"

       else
            do j = 1, total
     
                 read*, temp
                 meann = meann + temp
                 squared = squared + (temp)**2

            end do            

            meann = meann/total
            
            deriv = sqrt((squared/total)-(meann**2))

            print*, "mean: ", meann, "deviation: ", deriv
       end if                 
       end 
