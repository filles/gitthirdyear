            program advection_diffusion2d
                    !first Idea was to use the deriv module but
                    !was not sure how it translates to 2d since we would
                    !need to reimplement the entire function to get
                    !Jacobians or something
                use del2_mod 
                implicit none
                integer:: nx=1, ny=1, i, j
                real:: time=0., k=1.0, a_dif=0.1, a_adv=0.2 , dt_d, h, &
                curr=0., B =1., dt_min
                real,allocatable:: T(:,:), temp_T_2(:,:), S(:,:),&
                tempT(:,:)
                real,parameter :: pi =3.1415926

                ! in order to not make 2 Matricies we use the real
                ! part for vx and the imaginary for vy 
                ! may be possible to implement with pair or so made
                ! double since i thought my problems might be due to
                ! numerical issues
                double complex,allocatable :: dS(:,:)

                namelist /inputs/ nx,ny,B,k,a_adv, a_dif, time

                open(1,file="AdvDif_parameters.dat", status="old")
                read(1,inputs)
                close(1)

                allocate(T(nx,ny),temp_T_2(nx,ny), S(nx,ny), dS(nx,ny),&
                tempT(nx,ny))

                h = 1./(ny-1)
                dt_d = a_dif*(h**2)/k

                ! filling the S
                do i= 1,nx
                    do j= 1,ny
                        S(i,j) = B* sin(pi*i/nx)*sin(pi*j/ny)
                        !some initialisation which should be continuous
                        !satisfying the bdcs so we dont get super large
                        !derivatives at jumps
                        !T(i,j) = B* sin(pi*i/nx)*sin(2*pi*j/ny)
                    end do 
                end do
            !initialize T
               !T = 1.
               call random_number(T)
               !     do i= 1,nx
               !         do j= 1,ny
               !             T(i,j) = 0
               !             if((i == nx/2) .and. (j == ny/2)) T(i,j)= 1.
               !         end do 
               !     end do 
               T(:,1) = 1.
               T(:,ny) = 0

            ! calculate (vx and vy) dS from S
                call centeredfd(S,nx,ny,dS,h) 
            ! calculate dt_adv from vx, vy
                call upwind2d(S,nx,ny,dS,h, tempT)
            ! not sure if i am taking the right timesteps..
            dt_min = min(dt_d,a_adv*h/MAXVAL(tempT))
            !dt_min = min(dt_d,0.1*h)
            !dt_min = min(dt_dif,dt_adv)

               
            ! time = 0  solved since curr initialized as 0 

            ! Timestep
               i = 0
               do while (time > curr)

                    call upwind2d(T,nx,ny,dS,h, tempT)
                    call eeul2d(T,nx,ny,h,temp_T_2)
                    T = T + dt_min*(temp_T_2 - tempT )
                    T(1,:)= T (2,:)
                    T(nx,:) = T(nx-1,:)
                    curr = curr + dt_min


                    dt_min = min(dt_d,a_adv*h/MAXVAL(tempT))

                    !making sure we dont get into an endless loop 
                    if(dt_min <= 0) exit;

                    !if this condition is satisfied it is safe to assume
                    !we have reached the steadystate
                    if(tempT(5,5) - temp_T_2(5,5)<= 0.00000001 ) i = i+1
                    if( i == 100000) exit    

                end do 


                open(3,file='finalT_2d_B1.dat',status='replace')
                do j= 1,ny 
                   write(3,'(1000(1pe13.5))') T(:,j)
                end do
                close(3)
                
                deallocate(T,temp_T_2, S, dS, tempT)

            end program advection_diffusion2d 
