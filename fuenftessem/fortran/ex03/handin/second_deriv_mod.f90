            module second_deriv 
            contains
                
              subroutine second_derivative (y,n,dx,dydx_2)
                  integer,intent(in)  :: n
                  real,intent(in)     :: y(n), dx
                  real,intent(out)    :: dydx_2(n)
                  integer             :: i 

                  do i= 1,n-2
                      dydx_2(i+1) = (y(i+2)-2*y(i+1)+y(i))/(dx**2)
                  end do 

                  dydx_2(1)=0.
                  dydx_2(n) =0.
              end subroutine second_derivative 
                
            end module second_deriv 
