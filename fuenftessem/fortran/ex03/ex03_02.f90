            program mean_and_dev
                use mean_std_dev

                implicit none
                integer :: n,i
                real, allocatable :: a(:)
                real :: b, mean=0, stdderiv

            ! reading the input size
                open(1,file='asci.dat', status='old')
                n = 0
                do 
                    read(1,*,iostat=i) b
                    if(i<0) exit
                    if(i>0) stop 
                    n = n+1
                end do

                allocate(a(n))

                rewind(1)
                do i = 1,n
                    read(1,*) a(i)
                end do

            !calculating the mean and stddev
                stdderiv = stdderivation(a,n,mean)
                print*, "mean: ", mean, "stddeviation: ", stdderiv
                deallocate(a)

            end program mean_and_dev 

            
