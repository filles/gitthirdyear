         module mean_std_dev
         contains
             real  function stdderivation(a,n,mean)
              implicit none
              real,intent(in):: a(n)
              real,intent(out):: mean
              integer,intent(in):: n
              integer :: i
              real :: sums=0, sumsquared=0


             do i=1,n
                 sums = sums + a(i)
                 sumsquared =sumsquared +( a(i)**2)
             end do 
             mean = sums/n

             stdderivation = sqrt((sumsquared/n)-(mean**2))
            end function stdderivation 
          end module mean_std_dev 
