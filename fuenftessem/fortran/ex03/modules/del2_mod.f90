            module del2_mod
            contains

                subroutine del2 (T, nx, ny)
                    integer,intent(in)  :: nx, ny
                    real, intent(inout)    :: T(nx,ny)
                    real                :: Tnew(nx,ny)
                    integer             :: i, j 

                   Tnew = T 
                    do i= 2,nx-1 
                        do j= 2,ny-1
                            Tnew(i,j) = T(i,j) + dt*k*& 
          (T(i-1,j)+T(i+1,j)+T(i,j-1)+T(i,j+1)-4*T(i,j))/(h**2)*1.
                        end do 
                    end do
                   T = Tnew
                end subroutine del2

            end module del2_mod
