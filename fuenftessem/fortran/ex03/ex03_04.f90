            program diffusion2d
                    !first Idea was to use the deriv module but
                    !was not sure how it translates to 2d since we would
                    !need to reimplement the entire function to get
                    !Jacobians or something
                !use second_deriv
                implicit none
                integer:: nx=1, ny=1, i, j, nsteps
                real:: time=0., k=1.0, a=1, dt, h, curr=0.
                real,allocatable:: T(:,:), Tnew(:,:)
                character(len=5):: input="noise"

                namelist /inputs/ nx,ny,k,a,time,input

                open(1,file="parameters.dat", status="old")
                read(1,inputs)
                close(1)

                allocate(T(nx,ny),Tnew(nx,ny))
                h = 1./(ny-1)
                dt = a*(h**2)/k
                nsteps= time/dt


                if(input == "spike") then
                    do i= 1,nx
                        do j= 1,ny
                            T(i,j) = 0
                            if((i == nx/2) .and. (j == ny/2)) T(i,j)= 1.
                        end do 
                    end do 
                else
                    call random_number(T)
                    do i= 1,nx
                        T(i,1) = 0
                        T(i,ny) = 0
                    end do 
                    do i= 1,ny
                        T(1,i) = 0
                        T(ny,i) = 0
                    end do 
               end if 

                open(1,file='initialT_2d.dat')
                do j= 1,ny
                    write(1,'(1000(1pe13.5))') T(:,j)
                end do
                close(1)


            !Tnew is needed or else we would use new values in the
            !middle of one iteration 

                Tnew = T
                do while (time > curr)

            !since we assume 0 at boundaries we use i= 2, nx-1
            
                    do i= 2,nx-1 
                        do j= 2,ny-1
                            Tnew(i,j) = T(i,j) + dt*k*& 
          (T(i-1,j)+T(i+1,j)+T(i,j-1)+T(i,j+1)-4*T(i,j))/(h**2)*1.
                        end do 
                    end do
                    curr = curr + dt
                    T = Tnew
                end do 


                open(3,file='finalT_2d.dat',status='replace')
                do j= 1,ny 
                   write(3,'(1000(1pe13.5))') T(:,j)
                end do
                close(3)
                
                deallocate(T,Tnew)

            end program diffusion2d 
