            program diffusioneq 
                use second_deriv

                implicit none
                integer          :: i, no_p
                real, allocatable:: y(:), dydx_2(:), x(:)
                real             ::  dx,length, dt, time, curr=0 
                logical :: spike


                write(*,'(a,$)') "input number of Points on the Grid:"
                read*,no_p
                write(*,'(a,$)') "input length of domain:"
                read*,length 
                write(*,'(a,$)') "input length of time:"
                read*,time 

            !initialize

                allocate(y(no_p),dydx_2(no_p), x(no_p))
                dx = length/(no_p-1)
                dt = 0.4*dx**2


                
                write(*,'(a,$)') "initialize with a spike?(input &
                true/false)"
                read*,spike
               
                if(spike) then 
                    do i= 1,no_p
                        y(i) = 0
                        x(i) = (i-1)*dx     
                        if(i == no_p/2) y(i) = 1. 
                    end do 
                else
                   !initial version 
                   ! do i= 1,no_p
                   !     x(i) = (i-1)*dx     
                   ! !use of rng would be much better
                   !     y(i) = cos(i**(-0.5)*sin(i*0.4))**2 

                   ! end do 
                   call random_number(y)
                   y(1) = 0
                   y(no_p) = 0
                end if 
                    
                open(1,file='initialT_1d.dat')
                do i= 1,no_p 
                   write(1,*) y(i)
                end do
                close(1)

            !time step
                do while (time > curr)
                   call second_derivative(y,no_p,dx,dydx_2)
                    y = y + dt*dydx_2
                    curr = curr + dt
                end do 

            
                open(2,file='finalT_1d.dat')
                do i= 1,no_p 
                   write(2,*) y(i)
                end do
                close(2)
                
                deallocate(y, dydx_2, x)
            end program diffusioneq 

            
